<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutSubscriber implements EventSubscriberInterface
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public static function getSubscribedEvents()
    {
        return [
            LogoutEvent::class => 'addEmailAddressChangedHint'
        ];
    }

    public function addEmailAddressChangedHint(LogoutEvent $event)
    {
        if ('1' === $event->getRequest()->get('emailAddressChanged', '0'))
        {
            $event->setResponse(new RedirectResponse(
                $this->urlGenerator->generate('app_login', ['emailAddressChanged' => '1'])
            ));
        }
    }
}
