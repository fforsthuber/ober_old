<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    private UserPasswordEncoderInterface $passwordEncoder;
    private UuidFactoryInterface $uuidFactory;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        UuidFactoryInterface $uuidFactory
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->uuidFactory = $uuidFactory;
    }

    public function createFrontendUser(string $email, string $plainPassword): User
    {
        return $this->createUser($email, false, true, $plainPassword, [User::ROLE_USER]);
    }

    public function createLocationAdmin(string $email, array $locations, bool $isActive): User
    {
        $randomPassword = $this->uuidFactory->uuid4()->getHex()->toString();

        $user = $this->createUser($email, $isActive, false, $randomPassword, [User::ROLE_LOCATION_ADMIN]);

        foreach ($locations as $location) {
            $user->addLocation($location);
        }

        return $user;
    }

    public function createAppAdmin(string $email, bool $isActive): User
    {
        $randomPassword = $this->uuidFactory->uuid4()->getHex()->toString();

        return $this->createUser($email, $isActive, false, $randomPassword, [User::ROLE_ADMIN]);
    }

    private function createUser(
        string $email,
        bool $isActive,
        bool $mustVerifyEmailAddress,
        string $password,
        array $roles
    ): User {
        $user = new User(
            $email,
            $this->passwordEncoder->encodePassword(User::createEmpty(), $password),
            $roles,
            $this->uuidFactory->uuid4()->getHex()->toString()
        );

        if ($isActive) {
            $user->activate();
        }

        if (!$mustVerifyEmailAddress) {
            $user->setEmailAddressVerified();
        }

        return $user;
    }
}
