<?php

declare(strict_types=1);

namespace App\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResetPasswordMailer
{
    private string $fromEmailAddress;
    private MailerInterface $mailer;
    private array $supportedLocales;
    private TranslatorInterface $translator;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        string $fromEmailAddress,
        MailerInterface $mailer,
        array $supportedLocales,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->fromEmailAddress = $fromEmailAddress;
        $this->mailer = $mailer;
        $this->supportedLocales = $supportedLocales;
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
    }

    public function sendMail(string $toEmailAddress, string $locale, string $resetPasswordVerificationCode): void
    {
        if (!in_array($locale, $this->supportedLocales)) {
            throw new \InvalidArgumentException(sprintf('Locale %s is not supported', $locale));
        }

        $templateName = sprintf('email/forgot_password.%s.html.twig', $locale);
        $resetPasswordLink = $this->urlGenerator->generate(
            'reset_password',
            ['code' => $resetPasswordVerificationCode],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from($this->fromEmailAddress)
            ->to($toEmailAddress)
            ->subject($this->translator->trans('forgot_password_mail_subject', [], 'email'))
            ->htmlTemplate($templateName)
            ->context(['resetPasswordLink' => $resetPasswordLink])
        ;

        $this->mailer->send($email);
    }
}
