<?php

declare(strict_types=1);

namespace App\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailVerificationMailer
{
    private string $fromEmailAddress;
    private MailerInterface $mailer;
    private array $supportedLocales;
    private TranslatorInterface $translator;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(
        string $fromEmailAddress,
        MailerInterface $mailer,
        array $supportedLocales,
        TranslatorInterface $translator,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->fromEmailAddress = $fromEmailAddress;
        $this->mailer = $mailer;
        $this->supportedLocales = $supportedLocales;
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
    }

    public function sendMail(string $toEmailAddress, string $locale, string $emailAddressVerificationCode): void
    {
        if (!in_array($locale, $this->supportedLocales)) {
            throw new \InvalidArgumentException(sprintf('Locale %s is not supported', $locale));
        }

        $templateName = sprintf('email/email_verification.%s.html.twig', $locale);
        $verificationLink = $this->urlGenerator->generate(
            'verify_email_address',
            ['code' => $emailAddressVerificationCode],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $email = (new TemplatedEmail())
            ->from($this->fromEmailAddress)
            ->to($toEmailAddress)
            ->subject($this->translator->trans('email_verification_mail_subject', [], 'email'))
            ->htmlTemplate($templateName)
            ->context(['verificationLink' => $verificationLink])
        ;

        $this->mailer->send($email);
    }
}
