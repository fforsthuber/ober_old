<?php

declare(strict_types=1);

namespace App\Security;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidFactoryInterface;

class VerificationCodeUpdater
{
    private EntityManagerInterface $entityManager;
    private UserRepository $userRepository;
    private UuidFactoryInterface $uuidFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        UuidFactoryInterface $uuidFactory
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->uuidFactory = $uuidFactory;
    }

    public function updateByUserEmailAddress(string $emailAddress): string
    {
        if (!$user = $this->userRepository->findOneByEmailAddress($emailAddress)) {
            throw new \RuntimeException(sprintf('Could not find user by email address %s', $emailAddress));
        }

        $newVerificationCode = $this->uuidFactory->uuid4()->getHex()->toString();
        $user->applyVerificationCode($newVerificationCode);
        $this->entityManager->flush();

        return $newVerificationCode;
    }
}
