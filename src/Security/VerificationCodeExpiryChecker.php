<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;

class VerificationCodeExpiryChecker
{
    private int $verificationCodeTtlInSeconds;

    public function __construct(int $verificationCodeTtlInSeconds)
    {
        $this->verificationCodeTtlInSeconds = $verificationCodeTtlInSeconds;
    }

    public function checkExpiry(User $user)
    {
        $isCodeExpired =(new \DateTimeImmutable()) >
            $user->getVerificationCodeCreatedAt()->modify(sprintf('+%d seconds', $this->verificationCodeTtlInSeconds));

        if ($isCodeExpired) {
            throw new VerificationCodeExpiredException();
        }
    }
}
