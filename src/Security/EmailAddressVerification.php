<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailAddressVerification
{
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    public function verify(User $user): void
    {
        if ($user->isEmailAddressVerified()) {
            throw new EmailAddressAlreadyVerifiedException(
                $this->translator->trans('email_address_already_verified', [], 'frontend')
            );
        }

        $user->setEmailAddressVerified();
        $user->activate();
        $this->entityManager->flush();
    }
}
