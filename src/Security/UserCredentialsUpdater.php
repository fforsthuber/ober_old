<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserCredentialsUpdater
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;
    private TranslatorInterface $translator;
    private UuidFactoryInterface $uuidFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        TranslatorInterface $translator,
        UuidFactoryInterface $uuidFactory
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->translator = $translator;
        $this->uuidFactory = $uuidFactory;
    }

    public function updateEmailAddress(User $user, string $newEmailAddress): void
    {
        if ($newEmailAddress === $user->getEmail()) {
            return;
        }

        $user->updateEmail($newEmailAddress, (string) $this->uuidFactory->uuid4()->getHex());
        $this->entityManager->flush();
    }

    public function updatePassword(User $user, string $newPlainPassword): void
    {
        if (!$user->isPasswordResetRequested()) {
            throw new PasswordResetException(
                $this->translator->trans('password_reset_code_invalid', [], 'frontend')
            );
        }

        $user->updatePassword(
            $this->passwordEncoder->encodePassword($user, $newPlainPassword)
        );

        $this->entityManager->flush();
    }
}
