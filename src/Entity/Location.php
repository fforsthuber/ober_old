<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\LocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocationRepository::class)
 * @ORM\Table(indexes={
 *     @ORM\Index(name="street_city_idx", columns={"street", "city"})
 * })
 */
class Location
{
    use EntityUuid;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="locations")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Table", mappedBy="location", fetch="EXTRA_LAZY")
     */
    private $tables;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $street;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $streetNumber;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $zipCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $city;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    public function __construct(string $name, string $street, string $streetNumber, string $zipCode, string $city)
    {
        $this->id = $this->createUuid();
        $this->name = $name;
        $this->street = $street;
        $this->streetNumber = $streetNumber;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->users = new ArrayCollection();
    }

    public function addLocationAdmin(User $user): void
    {
        if (!in_array(User::ROLE_LOCATION_ADMIN, $user->getRoles())) {
            throw new \InvalidArgumentException(sprintf('user must have role %s', User::ROLE_LOCATION_ADMIN));
        }

        $this->users->add($user);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getStreetNumber(): string
    {
        return $this->streetNumber;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function isActive(): bool
    {
        return $this->active;
    }
}
