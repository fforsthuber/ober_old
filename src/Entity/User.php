<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`", indexes={
 *     @ORM\Index(name="verification_code_idx", columns={"verification_code"}),
 *     @ORM\Index(name="roles_idx", columns={"roles"})
 * })
 */
class User implements UserInterface
{
    private const ROLE_SEPARATOR = ',';

    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_LOCATION_ADMIN = 'ROLE_LOCATION_ADMIN';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    private $availableRoles = [
        self::ROLE_USER,
        self::ROLE_LOCATION_ADMIN,
        self::ROLE_ADMIN
    ];

    use EntityUuid;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Location", inversedBy="users")
     */
    private $locations;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $roles;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $hashedPassword;

    /**
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     options={"comment":"code to verify that data change was requested like new email address or changed password"}
     * )
     */
    private string $verificationCode;

    /**
     * @ORM\Column(
     *     type="datetime_immutable",
     *     options={"comment":"code creation timestamp use for expiry check"}
     * )
     */
    private \DateTimeImmutable $verificationCodeCreatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $emailAddressVerified = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $passwordResetRequested = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    public function __construct(
        string $email,
        string $hashedPassword,
        array $roles,
        string $verificationCode,
        ?\DateTimeImmutable $verificationCodeCreatedAt = null
    ) {
        $this->id = $this->createUuid();
        $this->email = $email;
        $this->applyVerificationCode($verificationCode);
        $this->hashedPassword = $hashedPassword;
        $this->locations = new ArrayCollection();
        $this->createdAt = $verificationCodeCreatedAt ?? new \DateTimeImmutable();

        if (empty($roles)) {
            throw new \InvalidArgumentException('At least one role must be provided');
        }

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    public static function createEmpty(): self
    {
        return new self('', '', [self::ROLE_USER], '');
    }

    public function updateEmail(string $email, string $emailVerificationCode): void
    {
        $this->email = $email;
        $this->applyVerificationCode($emailVerificationCode);
        $this->emailAddressVerified = false;
    }

    public function updatePassword(string $hashedPassword): void
    {
        $this->hashedPassword = $hashedPassword;
        $this->passwordResetRequested = false;
    }

    public function addLocation(Location $location): void
    {
        $location->addLocationAdmin($this);
        $this->locations->add($location);
    }

    public function addRole(string $role): void
    {
        $roles = $this->getRoles();

        if (in_array($role, $roles)) {
            return;
        }

        if (!in_array($role, $this->availableRoles)) {
            throw new \InvalidArgumentException(sprintf(
                'Role %s is not defined',
                $role
            ));
        }

        $roles[] = $role;
        $this->rolesToString($roles);
    }

    public function removeRole(string $roleToRemove): void
    {
        $roles = $this->getRoles();

        if (!in_array($roleToRemove, $roles)) {
            return;
        }

        if (1 === count($roles)) {
            throw new \DomainException(sprintf(
                'Can not remove single role %s from user. There must be at least one',
                $roleToRemove
            ));
        }

        $roles = array_filter($roles, function ($role) use ($roleToRemove) {
            return $role != $roleToRemove;
        });

        $this->rolesToString($roles);
    }

    public function setEmailAddressVerified(): void
    {
        $this->emailAddressVerified = true;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }

    public function requestPasswordReset(): void
    {
        $this->passwordResetRequested = true;
    }

    public function applyVerificationCode(string $verificationCode, ?\DateTimeImmutable $verificationCodeCreatedAt = null): void
    {
        $this->verificationCode = $verificationCode;
        $this->verificationCodeCreatedAt = $verificationCodeCreatedAt ?? new \DateTimeImmutable();
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return $this->rolesToArray();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->hashedPassword;
    }

    /**
     * @return string code to verify that data change was requested like new email address or changed password
     */
    public function getVerificationCode(): string
    {
        return $this->verificationCode;
    }

    /**
     * @return \DateTimeImmutable code creation timestamp use for expiry check
     */
    public function getVerificationCodeCreatedAt(): \DateTimeImmutable
    {
        return $this->verificationCodeCreatedAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    public function isEmailAddressVerified(): bool
    {
        return $this->emailAddressVerified;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function isPasswordResetRequested(): bool
    {
        return $this->passwordResetRequested;
    }

    private function rolesToArray(): array
    {
        return !empty($this->roles) ? explode(self::ROLE_SEPARATOR, $this->roles) : [];
    }

    private function rolesToString(array $roles): void
    {
        $this->roles = implode(self::ROLE_SEPARATOR, $roles);
    }
}
