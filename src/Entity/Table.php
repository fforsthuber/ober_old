<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TableRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TableRepository::class)
 */
class Table
{
    use EntityUuid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="tables")
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $capacity;

    public function __construct(Location $location, string $name, int $capacity)
    {
        $this->location = $location;
        $this->name = $name;
        $this->capacity = $capacity;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }
}
