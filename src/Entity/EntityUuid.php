<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Nonstandard\Uuid;
use Ramsey\Uuid\UuidInterface;

trait EntityUuid
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @var UuidInterface
     */
    private $id;

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    private function createUuid(): UuidInterface
    {
        return Uuid::uuid4();
    }
}
