<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->updatePassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOneByVerificationCode(string $verificationCode): ?User
    {
        $stmt = <<<DQL
            SELECT u FROM App\Entity\User u WHERE u.verificationCode = :verificationCode
        DQL;

        return $this->_em
            ->createQuery($stmt)
            ->setParameter('verificationCode', $verificationCode)
            ->getOneOrNullResult()
        ;
    }

    public function findOneByEmailAddress(string $emailAddress): ?User
    {
        $stmt = <<<DQL
            SELECT u FROM App\Entity\User u WHERE u.email = :email
        DQL;

        return $this->_em
            ->createQuery($stmt)
            ->setParameter('email', $emailAddress)
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return User[]
     */
    public function findByRole(string $role): array
    {
        $stmt = <<<DQL
            SELECT u FROM App\Entity\User u WHERE u.roles LIKE :role
        DQL;

        return $this->_em
            ->createQuery($stmt)
            ->setParameter('role', $role)
            ->getResult()
        ;
    }
}
