<?php

declare(strict_types=1);

namespace App\QueryBuilder;

use App\Entity\User;
use App\Pagination\PaginationQuery;
use App\Pagination\PaginationSettings;
use App\Pagination\Sort;
use Doctrine\ORM\EntityManagerInterface;

class AppAdminPaginationQueryBuilder
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createQuery(PaginationSettings $paginationSettings): PaginationQuery
    {
        $queryBuilder = $this->entityManager
            ->createQueryBuilder()
            ->select('u')
            ->from('App\Entity\User', 'u')
            ->where('u.roles LIKE :admin_role')
            ->setParameter('admin_role', User::ROLE_ADMIN)
        ;

        if (!empty($paginationSettings->getFilterQuery())) {
            $queryBuilder
                ->andWhere('u.email LIKE :filterQuery')
                ->setParameter('filterQuery', '%' . $paginationSettings->getFilterQuery() . '%')
            ;
        }

        $defaultSort = new Sort(['u.createdAt']);
        $defaultSort->addSortField('u.createdAt', Sort::SORT_DESC);

        return new PaginationQuery(
            'u.id',
            false,
            $queryBuilder,
            $paginationSettings,
            $defaultSort
        );
    }
}
