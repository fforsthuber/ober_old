<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\ResetPasswordType;
use App\Repository\UserRepository;
use App\Security\AutoLoginHandler;
use App\Security\PasswordResetException;
use App\Security\UserCredentialsUpdater;
use App\Security\VerificationCodeExpiredException;
use App\Security\VerificationCodeExpiryChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/resetPassword/{code}/", name="reset_password", methods={"GET", "POST"})
 */
class ResetPasswordController extends AbstractController
{
    public function __invoke(
        AutoLoginHandler $autoLoginHandler,
        string $code,
        Request $request,
        TranslatorInterface $translator,
        UserCredentialsUpdater $userCredentialsUpdater,
        UserRepository $userRepository,
        VerificationCodeExpiryChecker $verificationCodeExpiryChecker
    ): Response {

        if ($this->getUser()) {
            return $this->redirectToRoute('user_info');
        }

        $user = $userRepository->findOneByVerificationCode($code);

        if (!$user) {
            $this->addFlash('error_message', $translator->trans('password_reset_code_invalid', [], 'frontend'));
            return $this->redirectToRoute('app_login');
        }

        $resetPasswordForm = $this->createForm(ResetPasswordType::class)->handleRequest($request);

        if ($resetPasswordForm->isSubmitted() && $resetPasswordForm->isValid()) {
            try {
                $verificationCodeExpiryChecker->checkExpiry($user);
                $userCredentialsUpdater->updatePassword($user, $resetPasswordForm->getData()['password']);
                $this->addFlash('success_message', $translator->trans('reset_password_success', [], 'frontend'));
                return $autoLoginHandler->loginUser($request, $user);
            }
            catch (VerificationCodeExpiredException $e) {
                $this->addFlash('error_message', $translator->trans('password_reset_code_expired', [], 'frontend'));
                return $this->redirectToRoute('app_login');
            }
            catch (PasswordResetException $e) {
                $this->addFlash('error_message', $e->getMessage());
                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render(
            'security/reset_password.html.twig',
            [
                'form' => $resetPasswordForm->createView(),
            ]
        );
    }
}



