<?php

declare(strict_types=1);

namespace App\Controller\Admin\AppAdmin;

use App\Pagination\PaginationViewParameters;
use App\QueryBuilder\AppAdminPaginationQueryBuilder;
use App\Pagination\InvalidPaginationSettingException;
use App\Pagination\PaginationSettings;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/{_locale<%app.supported_locales_route%>}/appAdmin/list", name="admin_app_admin_list", methods={"GET", "POST"})
 */
class ListController extends AbstractController
{
    private AppAdminPaginationQueryBuilder $appAdminPaginatorBuilder;

    public function __construct(AppAdminPaginationQueryBuilder $appAdminPaginatorBuilder)
    {
        $this->appAdminPaginatorBuilder = $appAdminPaginatorBuilder;
    }

    public function __invoke(Request $request): Response
    {
        try {
            $allowedSortFields = ['u.email', 'u.createdAt', 'u.active'];
            $paginationQuery = $this->appAdminPaginatorBuilder->createQuery(
                PaginationSettings::createFromRequest($request, $allowedSortFields)
            );

            $paginationViewParameters =
                PaginationViewParameters::create($paginationQuery, $request)->toArray();
        } catch (InvalidPaginationSettingException $exception) {
            return $this->redirectToRoute('admin_app_admin_list', ['showNoResultsHint' => true]);
        }

        return $this->render(
            'admin/appAdmin/list.html.twig',
            $paginationViewParameters
        );
    }
}
