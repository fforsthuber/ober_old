<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\ForgotPasswordType;
use App\Mailer\ResetPasswordMailer;
use App\Repository\UserRepository;
use App\Security\VerificationCodeUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/forgotPassword/", name="forgot_password", methods={"GET", "POST"})
 */
class ForgotPasswordController extends AbstractController
{
    public function __invoke(
        EntityManagerInterface $entityManager,
        ResetPasswordMailer $resetPasswordMailer,
        Request $request,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        VerificationCodeUpdater $verificationCodeUpdater
    ): Response {

        if ($this->getUser()) {
            return $this->redirectToRoute('user_info');
        }

        $forgotPasswordForm = $this->createForm(ForgotPasswordType::class)->handleRequest($request);
        $emailAddressVerified = true;

        if ($forgotPasswordForm->isSubmitted() && $forgotPasswordForm->isValid()) {

            $user = $userRepository
                ->findOneByEmailAddress($forgotPasswordForm->getData()['email'])
            ;

            if ($emailAddressVerified = $user->isEmailAddressVerified()) {
                $newVerificationCode = $verificationCodeUpdater->updateByUserEmailAddress(
                    $forgotPasswordForm->getData()['email']
                );

                $resetPasswordMailer->sendMail(
                    $forgotPasswordForm->getData()['email'],
                    $request->getLocale(),
                    $newVerificationCode
                );

                $user->requestPasswordReset();
                $entityManager->flush();

                $this->addFlash('success_message', $translator->trans('reset_password_link_sent', [], 'frontend'));
                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render(
            'security/forgot_password.html.twig',
            [
                'form' => $forgotPasswordForm->createView(),
                'emailAddressVerified' => $emailAddressVerified,
            ]
        );
    }
}
