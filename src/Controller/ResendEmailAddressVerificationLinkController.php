<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\ResendVerifyEmailAddressLinkType;
use App\Mailer\EmailVerificationMailer;
use App\Security\VerificationCodeUpdater;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route(
 *     "/{_locale<%app.supported_locales_route%>}/resendVerifyEmailAddressLink/",
 *     name="resend_verify_email_address_link",
 *     methods={"GET", "POST"}
 * )
 */
class ResendEmailAddressVerificationLinkController extends AbstractController
{
    public function __invoke(
        VerificationCodeUpdater $verificationCodeUpdater,
        EmailVerificationMailer $emailVerificationMailer,
        Request $request,
        TranslatorInterface $translator
    ): Response {

        if ($this->getUser()) {
            return $this->redirectToRoute('user_info');
        }

        $resendLinkForm = $this->createForm(ResendVerifyEmailAddressLinkType::class)->handleRequest($request);

        if ($resendLinkForm->isSubmitted() && $resendLinkForm->isValid()) {
            try {
                $emailVerificationMailer->sendMail(
                    $resendLinkForm->getData()['email'],
                    $request->getLocale(),
                    $verificationCodeUpdater->updateByUserEmailAddress($resendLinkForm->getData()['email'])
                );
                $this->addFlash(
                    'success_message',
                    $translator->trans('resend_email_address_verification_link_success', [], 'frontend')
                );

                return $this->redirectToRoute('app_login');
            } catch (\RuntimeException $e) {
                $resendLinkForm->get('email')->addError(
                    new FormError($translator->trans('generic_error_message', [], 'frontend'))
                );
            }
        }

        return $this->render(
            'security/resend_email_address_verification_link.html.twig',
            [
                'form' => $resendLinkForm->createView(),
            ]
        );
    }
}
