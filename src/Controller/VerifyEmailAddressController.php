<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\UserRepository;
use App\Security\AutoLoginHandler;
use App\Security\EmailAddressAlreadyVerifiedException;
use App\Security\EmailAddressVerification;
use App\Security\EmailAddressVerificationException;
use App\Security\VerificationCodeExpiredException;
use App\Security\VerificationCodeExpiryChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/verifyEmailAddress/{code}/", name="verify_email_address")
 */
class VerifyEmailAddressController extends AbstractController
{
    public function __invoke(
        AutoLoginHandler $autoLoginHandler,
        string $code,
        EmailAddressVerification $emailAddressVerification,
        Request $request,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        VerificationCodeExpiryChecker $verificationCodeExpiryChecker
    ): Response {

        try{
            if (!$user = $userRepository->findOneByVerificationCode($code)) {
                throw new EmailAddressVerificationException(
                    $translator->trans('email_address_verification_code_invalid', [], 'frontend')
                );
            }

            $verificationCodeExpiryChecker->checkExpiry($user);
            $emailAddressVerification->verify($user);
            $this->addFlash('success_message', $translator->trans('email_address_verification_success', [], 'frontend'));

            return $autoLoginHandler->loginUser($request, $user);
        }
        catch (EmailAddressAlreadyVerifiedException $e) {
            $this->addFlash('error_message', $e->getMessage());
        }
        catch (VerificationCodeExpiredException $e) {
            $routeParams = ['showResendEmailAddressVerificationLink' => true];
            $this->addFlash('error_message', $translator->trans('email_address_verification_code_expired', [], 'frontend'));
        }
        catch (EmailAddressVerificationException $e) {
            $routeParams = ['showResendEmailAddressVerificationLink' => true];
            $this->addFlash('error_message', $e->getMessage());
        }

        return $this->redirectToRoute('app_login', $routeParams ?? []);
    }
}
