<?php

declare(strict_types=1);

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/info/", name="user_info", methods={"GET"})
 * @IsGranted("ROLE_USER")
 */
class InfoController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->render('user/info.html.twig');
    }
}
