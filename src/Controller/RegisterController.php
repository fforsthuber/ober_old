<?php

declare(strict_types=1);

namespace App\Controller;

use App\Factory\UserFactory;
use App\Form\Type\LoginType;
use App\Form\Type\RegisterType;
use App\Mailer\EmailVerificationMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/register/", name="user_register", methods={"GET", "POST"})
 */
class RegisterController extends AbstractController
{
    public function __invoke(
        EmailVerificationMailer $emailVerificationMailer,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Request $request,
        UserFactory $userFactory
    ): Response {
        if ($this->getUser()) {
            return $this->redirectToRoute('user_info');
        }

        $registerForm = $this->createForm(RegisterType::class)->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {
            $formData = $registerForm->getData();
            $user = $userFactory->createFrontendUser($formData['email'], $formData['password']);
            $entityManager->persist($user);
            $entityManager->flush();

            $emailVerificationMailer->sendMail($user->getEmail(), $request->getLocale(), $user->getVerificationCode());
            $this->addFlash('success_message', $translator->trans('registration_success', [], 'frontend'));
        }

        return $this->render(
            'security/login_register.html.twig',
            [
                'loginForm' => $this->createForm(LoginType::class)->createView(),
                'registerForm' => $registerForm->createView(),
            ]
        );
    }
}
