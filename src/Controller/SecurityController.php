<?php

namespace App\Controller;

use App\Form\Type\LoginType;
use App\Form\Type\RegisterType;
use App\Security\EmailAddressVerificationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/{_locale<%app.supported_locales_route%>}/login/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response {
        if ($this->getUser()) {
            return $this->redirectToRoute('user_info');
        }

        $lastAuthException = $authenticationUtils->getLastAuthenticationError();

        return $this->render(
            'security/login_register.html.twig',
            [
                'loginForm' => $this->createForm(LoginType::class)->createView(),
                'registerForm' => $this->createForm(RegisterType::class)->createView(),
                'loginError' => $lastAuthException ? $lastAuthException->getMessage() : '',
                'showResendEmailAddressVerificationLink' =>
                    $request->get('showResendEmailAddressVerificationLink', false) ||
                    ($lastAuthException && $lastAuthException instanceof EmailAddressVerificationException),
                'showEmailAddressChangedHint' => ('1' === $request->get('emailAddressChanged', '0'))
            ]
        );
    }

    /**
     * @Route("/logout/", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
