<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\UserDto;
use App\Entity\User;
use App\Form\Type\UpdateCredentialsType;
use App\Mailer\EmailVerificationMailer;
use App\Security\UserCredentialsUpdater;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale<%app.supported_locales_route%>}/updateCredentials/", name="update_credentials", methods={"GET", "POST"})
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class UpdateCredentialsController extends AbstractController
{
    public function __invoke(
        EmailVerificationMailer $emailVerificationMailer,
        Request $request,
        TranslatorInterface $translator,
        UserCredentialsUpdater $credentialsUpdater
    ): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $successMessage = '';
        $userDto = new UserDto($user->getEmail(), '');
        $updateCredentialsForm = $this
            ->createForm(UpdateCredentialsType::class, $userDto, ['user_id' => (string) $user->getId()])
            ->handleRequest($request)
        ;

        if ($updateCredentialsForm->isSubmitted() && $updateCredentialsForm->isValid()) {

            /** @var UserDto $credentialsData */
            $credentialsData = $updateCredentialsForm->getData();

            if(!empty($credentialsData->password) || !empty($credentialsData->email)) {
                $successMessage = $translator->trans('credentials_updated', [], 'frontend');
            }

            if(!empty($credentialsData->password)) {
                $user->requestPasswordReset();
                $credentialsUpdater->updatePassword($user, $credentialsData->password);
            }

            if(!empty($credentialsData->email) && $this->getUser()->getEmail() !== $credentialsData->email) {
                $credentialsUpdater->updateEmailAddress($user, $credentialsData->email);
                $emailVerificationMailer->sendMail($user->getEmail(), $request->getLocale(), $user->getVerificationCode());

                return $this->redirectToRoute('app_logout', ['emailAddressChanged' => '1']);
            }
        }

        return $this->render(
            'security/update_credentials.html.twig',
            [
                'form' => $updateCredentialsForm->createView(),
                'successMessage' => $successMessage,
            ]
        );
    }
}
