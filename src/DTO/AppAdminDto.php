<?php

declare(strict_types=1);

namespace App\DTO;

class AppAdminDto
{
    public ?string $id;
    public ?string $email;
    public ?bool $active;

    public function __construct(?string $id = null, ?string $email = null, ?bool $active = null)
    {
        $this->id = $id;
        $this->email = $email;
        $this->active = $active;
    }
}
