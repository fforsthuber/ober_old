<?php

declare(strict_types=1);

namespace App\Pagination;

use Symfony\Component\HttpFoundation\Request;

class PaginationSettings
{
    public const DEFAULT_MAX_RESULTS_PER_PAGE = 10;

    private string $filterQuery;
    private int $maxResultsPerPage;
    private int $offset;
    private int $page;
    private Sort $sort;

    /**
     * @throws InvalidPaginationSettingException
     */
    public function __construct(string $filterQuery, int $maxResultsPerPage, int $page, Sort $sort)
    {
        if ($maxResultsPerPage < 1) {
           throw new InvalidPaginationSettingException('maxResultsPerPage must be greater than 0');
        }

        if ($page < 1) {
            throw new InvalidPaginationSettingException('current page must be greater than 0');
        }

        if (strlen($filterQuery = urldecode(trim($filterQuery))) > 100) {
            throw new InvalidPaginationSettingException('filterQuery must not have more than 100 characters');
        }

        $this->filterQuery = $filterQuery;
        $this->maxResultsPerPage = $maxResultsPerPage;
        $this->offset = ($page - 1) * $maxResultsPerPage;
        $this->page = $page;
        $this->sort = $sort;
    }

    /**
     * @param array $allowedSortFields
     */
    public static function createFromRequest(Request $request, array $allowedSortFields): self
    {
        return new self(
            $request->get('filterQuery', ''),
            (int) $request->get('maxResultsPerPage', self::DEFAULT_MAX_RESULTS_PER_PAGE),
            (int) $request->get('page', 1),
            Sort::fromUrlSafeString($request->get('sort', ''), $allowedSortFields)
        );
    }

    public function getFilterQuery(): string
    {
        return $this->filterQuery;
    }

    public function getMaxResultsPerPage(): int
    {
        return $this->maxResultsPerPage;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getSort(): Sort
    {
        return $this->sort;
    }
}
