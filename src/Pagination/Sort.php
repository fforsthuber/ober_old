<?php

declare(strict_types=1);

namespace App\Pagination;

class Sort
{
    public const SORT_ASC = 'ASC';
    public const SORT_DESC = 'DESC';

    private const FIELD_SEPARATOR = '|';
    private const SORT_TYPE_SEPARATOR = '-';

    /**
     * @var string[]
     */
    private array $allowedSortFields;

    /**
     * @var string[]
     */
    private array $sortFields = [];

    /**
     * @param array $allowedSortFields
     */
    public function __construct(array $allowedSortFields)
    {
        $this->allowedSortFields = $allowedSortFields;
    }

    /**
     * @param array $allowedFields
     */
    public static function fromUrlSafeString(string $string, array $allowedFields): self
    {
        $instance = new self($allowedFields);

        if (empty($string) || strlen($string) > 300) {
            return $instance;
        }

        $sortFields = explode(self::FIELD_SEPARATOR, $string);

        foreach ($sortFields as $sortField) {
            $parts = explode(self::SORT_TYPE_SEPARATOR, $sortField);

            if (count($parts) > 1) {
                $instance->addSortField($parts[0], $parts[1]);
            }
        }

        return $instance;
    }

    public function addSortField(string $field, string $sortingTyp): void
    {
        if (in_array($field, $this->allowedSortFields) && in_array($sortingTyp, [self::SORT_ASC, self::SORT_DESC])) {
            $this->sortFields[$field] = $sortingTyp;
        }
    }

    /**
     * @return string[]
     */
    public function getSortFields(): array
    {
        return $this->sortFields;
    }

    public function toUrlSafeString(): string
    {
        $string = '';

        foreach ($this->sortFields as $key => $value) {
            $string .= $key . self::SORT_TYPE_SEPARATOR . $value . self::FIELD_SEPARATOR;
        }

        return rtrim($string, self::FIELD_SEPARATOR);
    }

    public function isEmpty(): bool
    {
        return empty($this->getSortFields());
    }
}
