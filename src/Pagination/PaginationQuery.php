<?php

declare(strict_types=1);

namespace App\Pagination;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

class PaginationQuery
{
    private string $countIdField;
    private QueryBuilder $countQueryBuilder;
    private bool $hasCollectionJoins;
    private Sort $defaultSort;
    private QueryBuilder $queryBuilder;
    private PaginationSettings $paginationSettings;

    /**
     * @param bool $hasCollectionJoins true if pagination query joins other tables with 'x-to-many' relations
     */
    public function __construct(
        string $countIdField,
        bool $hasCollectionJoins,
        QueryBuilder $queryBuilder,
        PaginationSettings $paginationSettings,
        Sort $defaultSort
    ) {
        $this->countIdField = $countIdField;
        $this->countQueryBuilder = clone $queryBuilder;
        $this->hasCollectionJoins = $hasCollectionJoins;
        $this->queryBuilder = $queryBuilder;
        $this->paginationSettings = $paginationSettings;
        $this->defaultSort = $defaultSort;
    }

    /**
     * @throws InvalidPaginationSettingException
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getMaxPage(): int
    {
        $resultCount = $this->countQueryBuilder
            ->select(sprintf('COUNT(%s)', $this->countIdField))
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return (int) ceil($resultCount / $this->paginationSettings->getMaxResultsPerPage());
    }

    public function getPaginationSettings(): PaginationSettings
    {
        return $this->paginationSettings;
    }

    public function getQuery(): Query
    {
        $this->queryBuilder
            ->setFirstResult($this->paginationSettings->getOffset())
            ->setMaxResults($this->paginationSettings->getMaxResultsPerPage())
        ;

        $sort = !$this->paginationSettings->getSort()->isEmpty() ?
            $this->paginationSettings->getSort() :
            $this->defaultSort
        ;

        foreach ($sort->getSortFields() as $sortField => $orderTyp) {
            $this->queryBuilder->addOrderBy($sortField, $orderTyp);
        }

        return $this->queryBuilder->getQuery();
    }

    public function hasCollectionJoins(): bool
    {
        return $this->hasCollectionJoins;
    }
}
