<?php

declare(strict_types=1);

namespace App\Pagination;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;

class PaginationViewParameters
{
    private PaginationQuery $paginationQuery;
    private Request $request;

    public static function create(PaginationQuery $paginationQuery, Request $request): self
    {
        $instance = new self();
        $instance->paginationQuery = $paginationQuery;
        $instance->request = $request;

        return $instance;
    }

    /**
     * @throws InvalidPaginationSettingException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function toArray(): array
    {
        $maxPage = $this->paginationQuery->getMaxPage();

        if ($this->paginationQuery->getPaginationSettings()->getPage() > $maxPage) {
            throw new InvalidPaginationSettingException('current page must not be greater than maxPage');
        }

        return [
            'filterQuery' => urldecode(trim($this->request->get('filterQuery', ''))),
            'page' => $this->request->get('page', 1),
            'maxPage' => $maxPage,
            'maxResultsPerPage' => $this->request->get('maxResultsPerPage', PaginationSettings::DEFAULT_MAX_RESULTS_PER_PAGE),
            'paginator' => new Paginator($this->paginationQuery->getQuery(), $this->paginationQuery->hasCollectionJoins()),
            'showNoResultsHint' => $this->request->get('showNoResultsHint', false),
        ];
    }
}
