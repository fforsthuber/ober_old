<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\User;
use App\Form\Constraint\UniqueEntityProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emailLengthConstraint = new Length(null, null, 180);
        $emailLengthConstraint->maxMessage = 'email_too_long';

        $passwordLengthConstraint = new Length(null, 8, 255);
        $passwordLengthConstraint->minMessage = 'password_too_short';
        $passwordLengthConstraint->maxMessage = 'password_too_long';

        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'attr' => ['placeholder' => 'placeholder_email'],
                    'constraints' => [
                        new NotBlank(),
                        $emailLengthConstraint,
                        new Email(),
                        new UniqueEntityProperty(User::class, 'email', 'email_exists')
                    ]
                ]
            )
            ->add('password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'passwords_dont_match',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => true,
                    'first_options'  => [
                        'constraints' => [
                            new NotBlank(),
                            $passwordLengthConstraint
                        ],
                        'attr' => ['placeholder' => 'placeholder_password']
                    ],
                    'second_options' => [
                        'attr' => ['placeholder' => 'placeholder_password_repeat']
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'frontend'
        ]);
    }
}
