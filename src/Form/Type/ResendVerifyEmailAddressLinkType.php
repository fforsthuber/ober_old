<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Form\Constraint\UserEmailAddressNotVerified;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ResendVerifyEmailAddressLinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emailLengthConstraint = new Length(null, null, 255);
        $emailLengthConstraint->maxMessage = 'email_too_long';

        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'attr' => ['placeholder' => 'placeholder_email'],
                    'constraints' => [
                        $emailLengthConstraint,
                        new Email(),
                        new UserEmailAddressNotVerified()
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'translation_domain' => 'frontend'
        ]);
    }
}
