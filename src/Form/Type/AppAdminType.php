<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\DTO\AppAdminDto;
use App\Entity\User;
use App\Form\Constraint\UniqueEntityProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AppAdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emailLengthConstraint = new Length(null, null, 180);
        $emailLengthConstraint->maxMessage = 'email_too_long';

        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'label' => 'Email',
                    'constraints' => [
                        new NotBlank(),
                        $emailLengthConstraint,
                        new Email(),
                        new UniqueEntityProperty(User::class, 'email', 'email_exists')
                    ]
                ]
            )
            ->add('active', CheckboxType::class, ['label' => 'aktiv'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AppAdminDto::class
        ]);
    }
}
