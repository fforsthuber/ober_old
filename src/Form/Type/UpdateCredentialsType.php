<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\DTO\UserDto;
use App\Entity\User;
use App\Form\Constraint\UniqueEntityProperty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

class UpdateCredentialsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emailLengthConstraint = new Length(null, null, 255);
        $emailLengthConstraint->maxMessage = 'email_too_long';

        $passwordLengthConstraint = new Length(null, 8, 255);
        $passwordLengthConstraint->minMessage = 'password_too_short';
        $passwordLengthConstraint->maxMessage = 'password_too_long';

        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => false,
                    'attr' => ['placeholder' => 'placeholder_new_email'],
                    'constraints' => [
                        $emailLengthConstraint,
                        new Email(),
                        new UniqueEntityProperty(User::class, 'email', 'email_exists', $options['user_id'])
                    ]
                ]
            )
            ->add('password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'passwords_dont_match',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => false,
                    'first_options'  => [
                        'attr' => ['placeholder' => 'placeholder_new_password'],
                        'constraints' => [
                            $passwordLengthConstraint
                        ],
                    ],
                    'second_options' => [
                        'attr' => ['placeholder' => 'placeholder_new_password_repeat']
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired('user_id')
            ->setAllowedTypes('user_id', 'string')
            ->setDefaults([
                'translation_domain' => 'frontend',
                'data_class' => UserDto::class,
            ])
        ;
    }
}
