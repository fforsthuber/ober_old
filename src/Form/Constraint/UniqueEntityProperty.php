<?php

declare(strict_types=1);

namespace App\Form\Constraint;

use Symfony\Component\Validator\Constraint;

class UniqueEntityProperty extends Constraint
{
    public string $entityClass;
    public string $entityProperty;
    public ?string $excludedEntityId;
    public string $message;

    public function __construct(string $entityClass, string $entityProperty, string $message, string $excludedEntityId = null)
    {
        parent::__construct();

        $this->entityClass = $entityClass;
        $this->entityProperty = $entityProperty;
        $this->excludedEntityId = $excludedEntityId;
        $this->message = $message;
    }
}
