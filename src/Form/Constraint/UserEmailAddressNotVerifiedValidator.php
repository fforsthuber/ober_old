<?php

declare(strict_types=1);

namespace App\Form\Constraint;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserEmailAddressNotVerifiedValidator extends ConstraintValidator
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserEmailAddressNotVerified $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UserEmailAddressNotVerified) {
            throw new \InvalidArgumentException(sprintf('given constraint must be of type %s', UserEmailAddressNotVerified::class));
        }

        if (empty($value)) {
            $this->context->buildViolation($constraint->emailAddressEmptyMessage)->addViolation();
            return;
        }

        if (!$user = $this->userRepository->findOneByEmailAddress($value)) {
            $this->context->buildViolation($constraint->emailAddressUnknownMessage)->addViolation();

            return;
        }

        if ($user->isEmailAddressVerified()) {
            $this->context->buildViolation($constraint->emailAddressAlreadyVerifiedMessage)->addViolation();
        }
    }
}
