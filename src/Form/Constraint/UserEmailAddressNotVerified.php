<?php

declare(strict_types=1);

namespace App\Form\Constraint;

use Symfony\Component\Validator\Constraint;

class UserEmailAddressNotVerified extends Constraint
{
    public string $emailAddressEmptyMessage = 'email_address_empty';
    public string $emailAddressUnknownMessage = 'email_address_unknown';
    public string $emailAddressAlreadyVerifiedMessage = 'email_address_already_verified';
}
