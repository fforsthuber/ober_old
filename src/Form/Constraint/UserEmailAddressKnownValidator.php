<?php

declare(strict_types=1);

namespace App\Form\Constraint;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserEmailAddressKnownValidator extends ConstraintValidator
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserEmailAddressKnown $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UserEmailAddressKnown) {
            throw new \InvalidArgumentException(sprintf('given constraint must be of type %s', UserEmailAddressKnown::class));
        }

        if (empty($value)) {
            $this->context->buildViolation($constraint->emailAddressEmptyMessage)->addViolation();
            return;
        }

        if (!$user = $this->userRepository->findOneByEmailAddress($value)) {
            $this->context->buildViolation($constraint->emailAddressUnknownMessage)->addViolation();
        }
    }
}
