<?php

declare(strict_types=1);

namespace App\Form\Constraint;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueEntityPropertyValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param UniqueEntityProperty $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueEntityProperty) {
            throw new \InvalidArgumentException(sprintf('given constraint must be of type %s', UniqueEntityProperty::class));
        }

        $entity = $this->entityManager
            ->getRepository($constraint->entityClass)
            ->findOneBy([$constraint->entityProperty => $value])
        ;

        if ($entity && $constraint->excludedEntityId !== (string) $entity->getId()) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
