export default class FrontendNavigation {

    private readonly navigationContainerId:string;

    constructor(navigationContainerId:any) {
        this.navigationContainerId = navigationContainerId;
    }

    public toggleMainNavigation(): void
    {
        let navigation = document.getElementById(this.navigationContainerId);

        if (navigation.className.includes('is-active')) {
            navigation.className = navigation.className.replace('is-active', '');
        } else {
            navigation.className += ' is-active';
        }
    }
}