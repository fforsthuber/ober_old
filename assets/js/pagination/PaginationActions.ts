import Pagination from "./Pagination";
import Sort from "./Sort";

export default class PaginationActions
{
    public init(pagination:Pagination): void
    {
        const tableElement = document.getElementById(Pagination.TABLE_ID);

        if (tableElement) {
            tableElement.querySelectorAll('th[data-sortField]').forEach((node) => {
                const sortField = node.getAttribute('data-sortField');

                this.setupSortButton(
                    node.querySelector('.sort-asc-btn'),
                    pagination,
                    sortField,
                    Sort.SORT_ASC
                );

                this.setupSortButton(
                    node.querySelector('.sort-desc-btn'),
                    pagination,
                    sortField,
                    Sort.SORT_DESC
                );

                this.setupSortClearButton(
                    node.querySelector('.sort-clear-btn'),
                    pagination,
                    sortField,
                )
            });

            this.setupPageChangeButton(
                document.querySelector('#previous-page-btn'),
                pagination.getPage() - 1,
                pagination
            )

            this.setupPageChangeButton(
                document.querySelector('#next-page-btn'),
                pagination.getPage() + 1,
                pagination
            )

            document.querySelectorAll('.set-page-btn').forEach((node) => {
                this.setupPageChangeButton(
                    node,
                    parseInt(node.textContent),
                    pagination
                )
            });

            this.setupTableSearch(
                document.querySelector('#table-search'),
                pagination
            );

            this.setupChangeMaxResultsPerPage(
                document.querySelector('#table-max-results-per-page'),
                pagination
            );
        }
    }

    private setupSortButton(buttonElement:Element, pagination:Pagination, sortField:string, sortType:string): void
    {
        if (buttonElement) {
            if (pagination.getSort().hasSortField(sortField, sortType)) {
                buttonElement.classList.add('sort-active');
            }

            buttonElement.addEventListener('click', (event) => {
                event.preventDefault();
                pagination.addSortField(sortField, sortType);
                pagination.refresh();
            });
        }
    }

    private setupSortClearButton(buttonElement:Element, pagination:Pagination, sortField:string): void
    {
        if (buttonElement) {
            buttonElement.addEventListener('click', (event) => {
                event.preventDefault();
                pagination.removeSortField(sortField);
                pagination.refresh();
            });
        }
    }

    private setupPageChangeButton(buttonElement:Element, page:number, pagination:Pagination): void
    {
        if (buttonElement) {
            buttonElement.addEventListener('click', (event) => {
                event.preventDefault();
                pagination.setPage(page);
                pagination.refresh();
            });
        }
    }

    private setupTableSearch(textField:HTMLInputElement, pagination:Pagination): void
    {
        if (textField) {
            textField.addEventListener('keypress', (event:KeyboardEvent) => {

                if (event.key === 'Enter') {
                    pagination.setFilterQuery(textField.value);
                    pagination.refresh();
                }
            });
        }
    }

    private setupChangeMaxResultsPerPage(textField:HTMLInputElement, pagination:Pagination): void
    {
        if (textField) {
            textField.addEventListener('keypress', (event:KeyboardEvent) => {

                if (event.key === 'Enter') {
                    pagination.setMaxResultsPerPage(parseInt(textField.value));
                    pagination.refresh();
                }
            });
        }
    }
}