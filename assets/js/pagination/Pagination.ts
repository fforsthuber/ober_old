import Sort from "./Sort";

export default class Pagination
{
    public static readonly TABLE_ID = 'pagination-table';

    private readonly PARAMETER_PAGE = 'page';
    private readonly PARAMETER_MAX_RESULTS_PER_PAGE = 'maxResultsPerPage';
    private readonly PARAMETER_FILTER_QUERY = 'filterQuery';
    private readonly PARAMETER_SORT = 'sort';
    private readonly FIELD_SEPARATOR = '|';
    private readonly SORT_TYPE_SEPARATOR = '-';

    private page:number = 1;
    private maxResultsPerPage:number = 10;
    private filterQuery:string = '';
    private sort:Sort = new Sort();


    public init(): void
    {
        const parameters = new URLSearchParams(window.location.search);
        const filterQuery = parameters.get(this.PARAMETER_FILTER_QUERY);
        const page = parseInt(parameters.get(this.PARAMETER_PAGE));
        const maxResultsPerPage = parseInt(parameters.get(this.PARAMETER_MAX_RESULTS_PER_PAGE));
        const sortString = parameters.get(this.PARAMETER_SORT);

        if (filterQuery && filterQuery.length > 0) {
            this.filterQuery = decodeURIComponent(filterQuery);
        }

        if (page > 0) {
            this.page = page;
        }

        if (maxResultsPerPage > 0) {
            this.maxResultsPerPage = maxResultsPerPage;
        }

        if (sortString && sortString.length > 0) {
            this.parseSort(sortString);
        }
    }

    public getSort(): Sort
    {
        return this.sort;
    }

    public getPage(): number
    {
        return this.page;
    }

    public setPage(page:number): void
    {
        if (page > 0) {
            this.page = page;
        }
    }

    public setMaxResultsPerPage(maxResultsPerPage:number): void
    {
        this.maxResultsPerPage = maxResultsPerPage;
    }

    public setFilterQuery(filterQuery:string): void
    {
        this.filterQuery = encodeURIComponent(filterQuery.trim());
    }

    public addSortField(sortField:string, sortType:string): void
    {
        this.sort.addSortField(sortField, sortType);
    }

    public removeSortField(sortField:string): void
    {
        this.sort.removeSortField(sortField);
    }

    public refresh(): void
    {
        const parameters = new URLSearchParams();

        if (this.filterQuery.length > 0) {
            parameters.append(this.PARAMETER_FILTER_QUERY, this.filterQuery);
        }

        parameters.append(this.PARAMETER_PAGE, this.page.toString());
        parameters.append(this.PARAMETER_MAX_RESULTS_PER_PAGE, this.maxResultsPerPage.toString());
        const sortString = this.createSortString();

        if (sortString.length > 0) {
            parameters.append(this.PARAMETER_SORT, sortString);
        }

        window.location.href = window.location.href.split('?')[0] + '?' + parameters.toString();
    }

    private parseSort(sortString:string): void
    {
        const fields = sortString.split(this.FIELD_SEPARATOR);

        for (const field of fields) {
            const parts = field.split(this.SORT_TYPE_SEPARATOR);

            if (parts.length > 1 && parts[0].length > 0) {
                this.sort.addSortField(parts[0], parts[1]);
            }
        }
    }

    private createSortString(): string
    {
        let sortString = '';

        this.sort.getSortFields().forEach((value, key) => {
            sortString += key + this.SORT_TYPE_SEPARATOR + value + this.FIELD_SEPARATOR;
        });

        return sortString.replace(/\|+$/, '');
    }
}