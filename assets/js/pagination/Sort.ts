export default class Sort
{
    public static readonly SORT_ASC = 'ASC';
    public static readonly SORT_DESC = 'DESC';

    private sortFields = new Map<string, string>();

    public addSortField(sortField:string, sortType:string): void
    {
        if ([Sort.SORT_ASC, Sort.SORT_DESC].includes(sortType)) {
            this.sortFields.set(sortField, sortType);
        }
    }

    public hasSortField(sortField: string, sortType:string): boolean
    {
        return this.sortFields.has(sortField) && sortType === this.sortFields.get(sortField)
    }

    public removeSortField(sortField:string): void
    {
        this.sortFields.delete(sortField)
    }

    public getSortFields(): Map<string, string>
    {
        return this.sortFields;
    }
}