require('@fortawesome/fontawesome-free/css/all.min.css');
import 'bulma/bulma.sass';
import './styles/backend.scss';
import Pagination from "./js/pagination/Pagination";
import PaginationActions from "./js/pagination/PaginationActions";

document.addEventListener('DOMContentLoaded', (event) => {
    if (undefined !== document.getElementById(Pagination.TABLE_ID)) {
        const pagination = new Pagination();
        const paginationActions = new PaginationActions();
        pagination.init();
        paginationActions.init(pagination);
    }
});