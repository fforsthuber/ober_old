require('@fortawesome/fontawesome-free/css/all.min.css');
import 'bulma/bulma.sass';
import './styles/frontend.scss';
import FrontendNavigation from "./js/navigation/FrontendNavigation";

document.addEventListener('DOMContentLoaded', (event) => {
    const frontendNavigation = new FrontendNavigation('main-navigation');
    const mainNaidationToggle = document.getElementById('main-navigation-toggle');

    if (mainNaidationToggle) {
        mainNaidationToggle.addEventListener('click', () => {
                frontendNavigation.toggleMainNavigation();
            }
        );
    }
});