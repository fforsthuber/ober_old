<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323060342 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE location (id UUID NOT NULL, name VARCHAR(255) NOT NULL, street VARCHAR(255) NOT NULL, street_number VARCHAR(10) NOT NULL, zip_code VARCHAR(10) NOT NULL, city VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5E9E89CB5E237E06 ON location (name)');
        $this->addSql('CREATE INDEX street_city_idx ON location (street, city)');
        $this->addSql('COMMENT ON COLUMN location.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "table" (id UUID NOT NULL, location_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, capacity SMALLINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F6298F4664D218E ON "table" (location_id)');
        $this->addSql('COMMENT ON COLUMN "table".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "table".location_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE user_location (user_id UUID NOT NULL, location_id UUID NOT NULL, PRIMARY KEY(user_id, location_id))');
        $this->addSql('CREATE INDEX IDX_BE136DCBA76ED395 ON user_location (user_id)');
        $this->addSql('CREATE INDEX IDX_BE136DCB64D218E ON user_location (location_id)');
        $this->addSql('COMMENT ON COLUMN user_location.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN user_location.location_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "table" ADD CONSTRAINT FK_F6298F4664D218E FOREIGN KEY (location_id) REFERENCES location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_location ADD CONSTRAINT FK_BE136DCBA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_location ADD CONSTRAINT FK_BE136DCB64D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ALTER roles TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE "user" ALTER roles DROP DEFAULT');
        $this->addSql('CREATE INDEX roles_idx ON "user" (roles)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "table" DROP CONSTRAINT FK_F6298F4664D218E');
        $this->addSql('ALTER TABLE user_location DROP CONSTRAINT FK_BE136DCB64D218E');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE "table"');
        $this->addSql('DROP TABLE user_location');
        $this->addSql('DROP INDEX roles_idx');
        $this->addSql('ALTER TABLE "user" ALTER roles TYPE JSON');
        $this->addSql('ALTER TABLE "user" ALTER roles DROP DEFAULT');
    }
}
