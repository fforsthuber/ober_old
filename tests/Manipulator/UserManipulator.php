<?php

declare(strict_types=1);

namespace App\Tests\Manipulator;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserManipulator
{
    private EntityManagerInterface $entityManager;
    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function modifyVerficationCodeCreatedAt(string $email, string $datetimeModifyString): void
    {
        $user = $this->userRepository->findOneByEmailAddress($email);

        if (!$user) {
            throw new \RuntimeException(sprintf('No user found by email %s', $email));
        }

        $user->applyVerificationCode(
            $user->getVerificationCode(),
            $user->getVerificationCodeCreatedAt()->modify($datetimeModifyString)
        );
        $this->entityManager->flush();
    }
}
