<?php

declare(strict_types=1);

namespace App\Tests\Fixture;

use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UsersFixture extends Fixture
{
    private UserFactory $userFactory;

    public function __construct(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    public function load(ObjectManager $manager)
    {
        $verifiedUser = $this->userFactory->createFrontendUser('verified@test.de', 'test1234');
        $verifiedUser->setEmailAddressVerified();
        $verifiedUser->activate();

        $unverifiedUser = $this->userFactory->createFrontendUser('unverified@test.de', 'test1234');
        $unverifiedUser->activate();

        $inactiveUser = $this->userFactory->createFrontendUser('deactivated@test.de', 'test1234');
        $inactiveUser->setEmailAddressVerified();
        $inactiveUser->deactivate();

        $manager->persist($verifiedUser);
        $manager->persist($unverifiedUser);
        $manager->persist($inactiveUser);
        $manager->flush();
    }
}
