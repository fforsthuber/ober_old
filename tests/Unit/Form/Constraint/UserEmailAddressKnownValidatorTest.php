<?php

declare(strict_types=1);

namespace App\Tests\Unit\Form\Constraint;

use App\Entity\User;
use App\Form\Constraint\UserEmailAddressKnown;
use App\Form\Constraint\UserEmailAddressKnownValidator;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class UserEmailAddressKnownValidatorTest extends ConstraintValidatorTestCase
{
    private User $userStub;

    public function setUp(): void
    {
        parent::setUp();
        $this->userStub = new User('test@test.de', 'hashedPW', [User::ROLE_USER], 'abcd');
    }

    public function test_that_InvalidArgumentException_is_thrown_on_invalid_constraint_type(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $validator = new UserEmailAddressKnownValidator($this->createMock(UserRepository::class));
        $validator->validate('test@test.de', new NotBlank());
    }

    public function test_that_violation_is_raised_when_no_entity_is_found_by_email(): void
    {
        $this->validator->validate(
            'unknown@test.de',
            new UserEmailAddressKnown()
        );
        $this->buildViolation('email_address_unknown')->assertRaised();
    }

    public function test_that_no_violation_is_raised_when_entity_is_found_by_email(): void
    {
        $this->validator->validate(
            'test@test.de',
            new UserEmailAddressKnown()
        );
        $this->assertNoViolation();
    }

    protected function createValidator()
    {
        $repositoryMock = $this->createMock(UserRepository::class);
        $repositoryMock
            ->method('findOneByEmailAddress')
            ->will($this->returnCallback(function ($email) {
                if ('test@test.de' === $email) {
                    return $this->userStub;
                }

                return null;
            }))
        ;

        return new UserEmailAddressKnownValidator($repositoryMock);
    }
}
