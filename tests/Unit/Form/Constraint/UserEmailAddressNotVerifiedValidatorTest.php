<?php

declare(strict_types=1);

namespace App\Tests\Unit\Form\Constraint;

use App\Entity\User;
use App\Form\Constraint\UserEmailAddressNotVerified;
use App\Form\Constraint\UserEmailAddressNotVerifiedValidator;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class UserEmailAddressNotVerifiedValidatorTest extends ConstraintValidatorTestCase
{
    private User $userWithoutVerifiedEmailStub;
    private User $userWithVerifiedEmailStub;

    public function setUp(): void
    {
        parent::setUp();
        $this->userWithoutVerifiedEmailStub = new User('test@test.de', 'hashedPW', [User::ROLE_USER], 'abcd');
        $this->userWithVerifiedEmailStub = new User('test@test.de', 'hashedPW', [User::ROLE_USER], 'abcd');
        $this->userWithVerifiedEmailStub->setEmailAddressVerified();
    }

    public function test_that_InvalidArgumentException_is_thrown_on_invalid_constraint_type(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $validator = new UserEmailAddressNotVerifiedValidator($this->createMock(UserRepository::class));
        $validator->validate('test@test.de', new NotBlank());
    }

    public function test_that_violation_is_raised_when_no_entity_is_found_by_email(): void
    {
        $this->validator->validate(
            'unknown@test.de',
            new UserEmailAddressNotVerified()
        );
        $this->buildViolation('email_address_unknown')->assertRaised();
    }

    public function test_that_violation_is_raised_when_email_is_already_verified(): void
    {
        $this->validator->validate(
            'verified@test.de',
            new UserEmailAddressNotVerified()
        );
        $this->buildViolation('email_address_already_verified')->assertRaised();
    }

    public function test_that_no_violation_is_raised_when_email_is_not_yet_verified(): void
    {
        $this->validator->validate(
            'not-verified@test.de',
            new UserEmailAddressNotVerified()
        );
        $this->assertNoViolation();
    }

    protected function createValidator()
    {
        $repositoryMock = $this->createMock(UserRepository::class);
        $repositoryMock
            ->method('findOneByEmailAddress')
            ->will($this->returnCallback(function ($email) {
                if ('verified@test.de' === $email) {
                    return $this->userWithVerifiedEmailStub;
                }

                if ('not-verified@test.de' === $email) {
                    return $this->userWithoutVerifiedEmailStub;
                }

                return null;
            }))
        ;

        return new UserEmailAddressNotVerifiedValidator($repositoryMock);
    }

}
