<?php

declare(strict_types=1);

namespace App\Tests\Unit\Form\Constraint;

use App\Entity\User;
use App\Form\Constraint\UniqueEntityProperty;
use App\Form\Constraint\UniqueEntityPropertyValidator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class UniqueEntityPropertyValidatorTest extends ConstraintValidatorTestCase
{
    private User $userStub;

    public function setUp(): void
    {
        parent::setUp();
        $this->userStub = new User('test@test.de', 'hashedPW', [User::ROLE_USER], 'abcd');
    }

    public function test_that_InvalidArgumentException_is_thrown_on_invalid_constraint_type(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $validator = new UniqueEntityPropertyValidator($this->createMock(EntityManagerInterface::class));
        $validator->validate('test@test.de', new NotBlank());
    }

    public function test_that_duplicate_entity_is_detected(): void
    {
        $this->validator->validate(
            'test@test.de',
            new UniqueEntityProperty(User::class, 'email', 'duplicate_email')
        );
        $this->buildViolation('duplicate_email')->assertRaised();
    }

    public function test_that_no_duplicate_entity_violation_is_raised_with_same_entity_id(): void
    {
        $this->validator->validate(
            'test@test.de',
            new UniqueEntityProperty(User::class, 'email', 'duplicate_email', $this->userStub->getId()->toString())
        );

        $this->assertNoViolation();
    }

    public function test_that_no_duplicate_entity_violation_is_raised_on_non_duplicate_property_value(): void
    {
        $this->validator->validate(
            'new-email@test.de',
            new UniqueEntityProperty(User::class, 'email', 'duplicate_email')
        );

        $this->assertNoViolation();
    }

    protected function createValidator()
    {
        $repositoryMock = $this->createMock(ObjectRepository::class);
        $repositoryMock
            ->method('findOneBy')
            ->will($this->returnCallback(function ($params) {
                if (isset($params['email']) && 'test@test.de' === $params['email']) {
                    return $this->userStub;
                }

                return null;
            }))
        ;

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock
            ->method('getRepository')
            ->with(User::class)
            ->willReturn($repositoryMock)
        ;

        return new UniqueEntityPropertyValidator($entityManagerMock);
    }
}
