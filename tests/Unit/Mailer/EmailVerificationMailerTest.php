<?php

declare(strict_types=1);

namespace App\Tests\Unit\Mailer;

use App\Mailer\EmailVerificationMailer;
use App\Tests\Stub\MailerStub;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailVerificationMailerTest extends TestCase
{
    public function test_that_email_address_verification_email_is_sent(): void
    {
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->method('trans')->will($this->returnCallback(function($param) {
            if ('email_verification_mail_subject' === $param) {
                return 'Verify your email address';
            }

            return '';
        }));

        $urlGeneratorMock = $this->createMock(UrlGeneratorInterface::class);
        $urlGeneratorMock->method('generate')->will($this->returnCallback(function($param1, $param2) {
            if ('verify_email_address' === $param1 && isset($param2['code'])) {
                return sprintf('https://ober.test/verifyEmailAddress/%s', $param2['code']);
            }

            return '';
        }));

        $mailerStub = new MailerStub();
        $mailer = new EmailVerificationMailer(
            'from@test.de',
            $mailerStub,
            ['en'],
            $translatorMock,
            $urlGeneratorMock
        );

        $mailer->sendMail('to@test.de', 'en', 'abcd123');
        $sentMessage = $mailerStub->getSentMessage();

        $this->assertSame('from@test.de', $sentMessage->getFrom()[0]->getAddress());
        $this->assertSame('to@test.de', $sentMessage->getTo()[0]->getAddress());
        $this->assertSame('Verify your email address', $sentMessage->getSubject());
        $this->assertSame('email/email_verification.en.html.twig', $sentMessage->getHtmlTemplate());
        $this->assertSame(
            ['verificationLink' => 'https://ober.test/verifyEmailAddress/abcd123'],
            $sentMessage->getContext()
        );
    }

    public function test_that_InvalidArgumentException_is_thrown_on_unsupported_locale(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $mailer = new EmailVerificationMailer(
            'from@test.de',
            new MailerStub(),
            ['en'],
            $this->createMock(TranslatorInterface::class),
            $this->createMock(UrlGeneratorInterface::class)
        );
        $mailer->sendMail('to@test.de', 'de', 'abcd123');
    }
}
