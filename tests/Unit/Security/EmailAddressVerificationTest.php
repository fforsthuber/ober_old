<?php

declare(strict_types=1);

namespace App\Tests\Unit\Security;

use App\Entity\User;
use App\Security\EmailAddressAlreadyVerifiedException;
use App\Security\EmailAddressVerification;
use App\Security\EmailAddressVerificationException;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailAddressVerificationTest extends TestCase
{
    public function test_that_EmailAddressVerificationException_is_thrown_on_already_verified_email_address(): void
    {
        $this->expectException(EmailAddressAlreadyVerifiedException::class);
        $this->expectExceptionMessage('email_address_already_verified');

        $emailAddressVerification = new EmailAddressVerification(
            $this->createMock(EntityManagerInterface::class),
            $this->getTranslatorStub(),
            3600
        );

        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );
        $user->setEmailAddressVerified();

        $emailAddressVerification->verify($user);
    }
    
    private function getTranslatorStub(): TranslatorInterface
    {
        $translatorStub = $this->createMock(TranslatorInterface::class);
        $translatorStub->method('trans')->will($this->returnCallback(function ($param) {
            return $param;
        }));
        
        return $translatorStub;
    }
}
