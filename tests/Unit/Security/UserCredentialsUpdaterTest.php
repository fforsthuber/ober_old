<?php

declare(strict_types=1);

namespace App\Tests\Unit\Security;

use App\Entity\User;
use App\Security\PasswordResetException;
use App\Security\UserCredentialsUpdater;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserCredentialsUpdaterTest extends TestCase
{
    public function test_that_PasswordResetException_if_no_password_reset_was_requested()
    {
        $this->expectException(PasswordResetException::class);
        $this->expectExceptionMessage('password_reset_code_invalid');

        $userCredentialsUpdater = new UserCredentialsUpdater(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(UserPasswordEncoderInterface::class),
            $this->getTranslatorStub(),
            $this->createMock(UuidFactoryInterface::class),
            3600
        );

        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $userCredentialsUpdater->updatePassword($user, 'newPassword');
    }

    private function getTranslatorStub(): TranslatorInterface
    {
        $translatorStub = $this->createMock(TranslatorInterface::class);
        $translatorStub->method('trans')->will($this->returnCallback(function ($param) {
            return $param;
        }));

        return $translatorStub;
    }
}
