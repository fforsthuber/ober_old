<?php

declare(strict_types=1);

namespace App\Tests\Unit\Security;

use App\Entity\User;
use App\Security\VerificationCodeExpiredException;
use App\Security\VerificationCodeExpiryChecker;
use PHPUnit\Framework\TestCase;

class VerificationCodeExpiryCheckerTest extends TestCase
{
    public function test_that_VerificationCodeExpiredException_is_thrown_on_outdated_verification_code ()
    {
        $this->expectException(VerificationCodeExpiredException::class);

        $verificationCodeExpiryChecker = new VerificationCodeExpiryChecker(1);

        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );
        $user->requestPasswordReset();
        $user->applyVerificationCode($user->getVerificationCode(), $user->getVerificationCodeCreatedAt()->modify('-2 seconds'));

        $verificationCodeExpiryChecker->checkExpiry($user);
    }
}
