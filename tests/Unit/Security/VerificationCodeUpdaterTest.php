<?php

declare(strict_types=1);

namespace App\Tests\Unit\Security;

use App\Repository\UserRepository;
use App\Security\VerificationCodeUpdater;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\UuidFactoryInterface;

class VerificationCodeUpdaterTest extends TestCase
{
    public function test_that_RuntimeException_is_thrown_if_no_user_is_found_by_email_address(): void
    {
        $this->expectException(\RuntimeException::class);
        $userRepositoryStub = $this->createMock(UserRepository::class);
        $userRepositoryStub->method('findOneByEmailAddress')->willReturn(null);

        $verificationCodeUpdater = new VerificationCodeUpdater(
            $this->createMock(EntityManagerInterface::class),
            $userRepositoryStub,
            $this->createMock(UuidFactoryInterface::class)
        );

        $verificationCodeUpdater->updateByUserEmailAddress('unknown@test.de');
    }
}
