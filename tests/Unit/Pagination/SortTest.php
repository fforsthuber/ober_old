<?php

declare(strict_types=1);

namespace App\Tests\Unit\Pagination;

use App\Pagination\Sort;
use PHPUnit\Framework\TestCase;

class SortTest extends TestCase
{
    public function test_adding_sort_field_success(): void
    {
        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field1', Sort::SORT_ASC);
        $sort->addSortField('field2', Sort::SORT_DESC);

        $this->assertSame(
            ['field1' => Sort::SORT_ASC, 'field2' => Sort::SORT_DESC],
            $sort->getSortFields()
        );
    }

    public function test_overwrite_existing_sort_field(): void
    {
        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field1', Sort::SORT_ASC);
        $sort->addSortField('field1', Sort::SORT_DESC);

        $this->assertSame(
            ['field1' => Sort::SORT_DESC],
            $sort->getSortFields()
        );
    }

    public function test_adding_not_allowed_sort_field_fail(): void
    {
        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field3', Sort::SORT_ASC);

        $this->assertTrue($sort->isEmpty());
    }

    public function test_adding_sort_field_with_unknown_sort_type_fail(): void
    {
        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field1', 'UNKNOWN');

        $this->assertTrue($sort->isEmpty());
    }

    public function test_that_toUrlSafeString_outputs_correct_string(): void
    {
        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field1', Sort::SORT_ASC);
        $sort->addSortField('field2', Sort::SORT_DESC);

        $this->assertSame(
            'field1-ASC|field2-DESC',
            $sort->toUrlSafeString()
        );
    }

    public function test_that_url_safe_string_is_parsed_correctly(): void
    {
        $expectedSort = new Sort(['field1', 'field2']);
        $expectedSort->addSortField('field1', Sort::SORT_ASC);
        $expectedSort->addSortField('field2', Sort::SORT_DESC);

        $this->assertEquals(
            $expectedSort,
            Sort::fromUrlSafeString('field1-ASC|field2-DESC', ['field1', 'field2'])
        );
    }

    public function test_that_fromUrlSafeString_trims_whitespaces_and_separators(): void
    {
        $expectedSort = new Sort(['field1', 'field2']);
        $expectedSort->addSortField('field1', Sort::SORT_ASC);
        $expectedSort->addSortField('field2', Sort::SORT_DESC);

        $this->assertEquals(
            $expectedSort,
            Sort::fromUrlSafeString(' -|field1-ASC|-||field2-DESC|-| ', ['field1', 'field2'])
        );
    }

    public function test_that_fromUrlSafeString_ignores_not_allowed_fields_and_sort_types(): void
    {
        $expectedSort = new Sort(['field1', 'field2', 'field3']);
        $expectedSort->addSortField('field1', Sort::SORT_ASC);
        $expectedSort->addSortField('field2', Sort::SORT_DESC);

        $this->assertEquals(
            $expectedSort,
            Sort::fromUrlSafeString('field1-ASC|field2-DESC|field3-UNKNOWN|unknown-ASC', ['field1', 'field2', 'field3'])
        );
    }

    public function test_that_fromUrlSafeString_handles_empty_strings(): void
    {
        $this->assertEquals(
            new Sort(['field1', 'field2']),
            Sort::fromUrlSafeString('', ['field1', 'field2'])
        );
    }

    public function test_that_fromUrlSafeString_does_not_parse_strings_bigger_than_300_chars(): void
    {
        $this->assertEquals(
            new Sort(['field1', 'field2']),
            Sort::fromUrlSafeString(str_repeat('field1-ASC|field2-DESC|', 15), ['field1', 'field2'])
        );
    }
}
