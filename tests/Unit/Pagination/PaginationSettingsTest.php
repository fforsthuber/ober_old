<?php

declare(strict_types=1);

namespace App\Tests\Unit\Pagination;

use App\Pagination\InvalidPaginationSettingException;
use App\Pagination\PaginationSettings;
use App\Pagination\Sort;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class PaginationSettingsTest extends TestCase
{
    public function test_that_settings_object_is_created_correctly_from_request(): void
    {
        $request = new Request([
            'filterQuery' => 'test query',
            'maxResultsPerPage' => 15,
            'page' => 3,
            'sort' => 'field1-ASC|field2-DESC',
        ]);

        $paginationSettings = PaginationSettings::createFromRequest($request, ['field1', 'field2']);
        $expectedSort = new Sort(['field1', 'field2']);
        $expectedSort->addSortField('field1', Sort::SORT_ASC);
        $expectedSort->addSortField('field2', Sort::SORT_DESC);

        $this->assertSame('test query', $paginationSettings->getFilterQuery());
        $this->assertSame(15, $paginationSettings->getMaxResultsPerPage());
        $this->assertSame(3, $paginationSettings->getPage());
        $this->assertSame(30, $paginationSettings->getOffset());
        $this->assertEquals($expectedSort, $paginationSettings->getSort());
    }

    public function test_that_settings_object_created_from_request_has_correct_defaults(): void
    {
        $paginationSettings = PaginationSettings::createFromRequest(new Request(), ['field1', 'field2']);

        $this->assertSame('', $paginationSettings->getFilterQuery());
        $this->assertSame(PaginationSettings::DEFAULT_MAX_RESULTS_PER_PAGE, $paginationSettings->getMaxResultsPerPage());
        $this->assertSame(1, $paginationSettings->getPage());
        $this->assertSame(0, $paginationSettings->getOffset());
        $this->assertEquals(new Sort(['field1', 'field2']), $paginationSettings->getSort());
    }

    public function test_that_exception_is_thrown_on_maxResultsPerPage_lte_1(): void
    {
        $this->expectException(InvalidPaginationSettingException::class);
        $this->expectExceptionMessage('maxResultsPerPage must be greater than 0');

        new PaginationSettings('', 0, 1, new Sort(['field1', 'field2']));
    }

    public function test_that_exception_is_thrown_on_page_lte_1(): void
    {
        $this->expectException(InvalidPaginationSettingException::class);
        $this->expectExceptionMessage('current page must be greater than 0');

        new PaginationSettings('', 1, 0, new Sort(['field1', 'field2']));
    }

    public function test_that_exception_is_thrown_on_filterQuery_gte_100(): void
    {
        $this->expectException(InvalidPaginationSettingException::class);
        $this->expectExceptionMessage('filterQuery must not have more than 100 characters');

        new PaginationSettings(str_repeat('a', 101), 1, 1, new Sort(['field1', 'field2']));
    }
}
