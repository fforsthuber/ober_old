<?php

declare(strict_types=1);

namespace App\Tests\Unit\Pagination;

use App\Pagination\PaginationQuery;
use App\Pagination\PaginationSettings;
use App\Pagination\Sort;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use PHPUnit\Framework\TestCase;

class PaginationQueryTest extends TestCase
{
    public function test_that_getMaxPage_returns_expected_result(): void
    {
        $queryMock = $this->createMock(AbstractQuery::class);
        $queryMock->method('getSingleScalarResult')->willReturn(120);

        $queryBuilderMock = $this->createMock(QueryBuilder::class);
        $queryBuilderMock
            ->expects($this->once())
            ->method('select')
            ->with('COUNT(u.id)')
            ->willReturn($queryBuilderMock)
        ;
        $queryBuilderMock->method('getQuery')->willReturn($queryMock);

        $paginationQuery = new PaginationQuery(
            'u.id',
            false,
            $queryBuilderMock,
            new PaginationSettings('', 10, 1, new Sort(['field1', 'field2'])),
            new Sort(['field1', 'field2'])
        );

        $this->assertSame(12, $paginationQuery->getMaxPage());
    }

    public function test_that_created_query_has_expected_defaults(): void
    {
        $entityMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getConfiguration'])
            ->getMock()
        ;
        $entityMock->method('getConfiguration')->willReturn(new Configuration());

        $queryBuilder = new QueryBuilder($entityMock);

        $paginationQuery = new PaginationQuery(
            'u.id',
            false,
            $queryBuilder,
            new PaginationSettings('', 10, 1, new Sort(['field1', 'field2'])),
            new Sort(['field1', 'field2'])
        );
        $query = $paginationQuery->getQuery();

        $this->assertSame(0, $query->getFirstResult());
        $this->assertSame(10, $query->getMaxResults());
    }

    public function test_that_created_query_reflects_given_pagination_settings(): void
    {
        $entityMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getConfiguration'])
            ->getMock()
        ;
        $entityMock->method('getConfiguration')->willReturn(new Configuration());

        $queryBuilder = new QueryBuilder($entityMock);

        $sort = new Sort(['field1', 'field2']);
        $sort->addSortField('field1', Sort::SORT_DESC);
        $sort->addSortField('field2', Sort::SORT_ASC);

        $paginationQuery = new PaginationQuery(
            'u.id',
            false,
            $queryBuilder,
            new PaginationSettings('', 15, 3, $sort),
            new Sort(['field1', 'field2'])
        );
        $query = $paginationQuery->getQuery();

        $this->assertSame(30, $query->getFirstResult());
        $this->assertSame(15, $query->getMaxResults());
        $this->assertStringContainsString('ORDER BY field1 DESC, field2 ASC', $query->getDQL());
    }
}
