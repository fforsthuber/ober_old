<?php

declare(strict_types=1);

namespace App\Tests\Unit\Factory;

use App\Entity\Location;
use App\Entity\User;
use App\Factory\UserFactory;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Rfc4122\UuidV4;
use Ramsey\Uuid\UuidFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactoryTest extends TestCase
{
    public function test_that_frontend_user_is_created_as_expected(): void
    {
        $verificationCode = 'a147690733bc43d9976b6335580fd790';

        $userStub = new User(
            'test@test.de',
            'encodedPassword',
            [User::ROLE_USER],
            $verificationCode
        );

        $factory = new UserFactory(
            $this->getPasswordEncoderMock(),
            $this->createUuidFactoryMock($verificationCode)
        );
        $frontendUser = $factory->createFrontendUser('test@test.de', 'plainPassword');

        $this->assertFalse($frontendUser->isActive());
        $this->assertFalse($frontendUser->isEmailAddressVerified());
        $this->assertSame($userStub->getEmail(), $frontendUser->getEmail());
        $this->assertSame($userStub->getPassword(), $frontendUser->getPassword());
        $this->assertSame($userStub->getRoles(), $frontendUser->getRoles());
        $this->assertSame($userStub->getVerificationCode(), $frontendUser->getVerificationCode());
    }

    public function test_that_location_admin_is_created_correctly(): void
    {
        $verificationCode = 'a147690733bc43d9976b6335580fd790';

        $userStub = new User(
            'test@test.de',
            'encodedPassword',
            [User::ROLE_LOCATION_ADMIN],
            $verificationCode
        );

        $factory = new UserFactory(
            $this->getPasswordEncoderMock(),
            $this->createUuidFactoryMock($verificationCode)
        );
        $locationAdmin = $factory->createLocationAdmin(
            'test@test.de',
            [new Location('test location', 'Teststreet', '1', '12345', 'Testcity')],
            true
        );

        $this->assertTrue($locationAdmin->isActive());
        $this->assertTrue($locationAdmin->isEmailAddressVerified());
        $this->assertSame($userStub->getEmail(), $locationAdmin->getEmail());
        $this->assertSame($userStub->getPassword(), $locationAdmin->getPassword());
        $this->assertSame($userStub->getRoles(), $locationAdmin->getRoles());
        $this->assertSame($userStub->getVerificationCode(), $locationAdmin->getVerificationCode());
    }

    private function getPasswordEncoderMock(): UserPasswordEncoderInterface
    {
        $passwordEncoderMock = $this->createMock(UserPasswordEncoderInterface::class);
        $passwordEncoderMock->method('encodePassword')->willReturn('encodedPassword');

        return $passwordEncoderMock;
    }

    private function createUuidFactoryMock(string $verificationCode): UuidFactoryInterface
    {
        $uuidFactoryMock = $this->createMock(UuidFactoryInterface::class);
        $uuidFactoryMock->method('uuid4')->willReturn(UuidV4::fromString($verificationCode));

        return $uuidFactoryMock;
    }
}
