<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class UserTest extends TestCase
{
    public function test_that_user_is_created_as_expected(): void
    {
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $this->assertTrue(
            Uuid::UUID_TYPE_RANDOM === $user->getId()->getFields()->getVersion(),
            'assert UUIDv4'
        );

        $this->assertSame('test@test.de', $user->getEmail());
        $this->assertSame('abcdef1234567890', $user->getPassword()); //get hashed password
        $this->assertSame('123abc', $user->getVerificationCode());

        $this->assertGreaterThan((new \DateTimeImmutable())->modify('-1 second'), $user->getCreatedAt());
        $this->assertLessThan((new \DateTimeImmutable())->modify('+1 second'), $user->getCreatedAt());

        $this->assertGreaterThan((new \DateTimeImmutable())->modify('-1 second'), $user->getVerificationCodeCreatedAt());
        $this->assertLessThan((new \DateTimeImmutable())->modify('+1 second'), $user->getVerificationCodeCreatedAt());

        $this->assertSame([User::ROLE_USER], $user->getRoles());
        $this->assertFalse($user->isPasswordResetRequested());
        $this->assertFalse($user->isActive());
        $this->assertFalse($user->isEmailAddressVerified());
    }

    public function test_that_InvalidArgument_exception_is_thrown_on_empty_rules_array(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new User(
            'test@test.de',
            'abcdef1234567890',
            [],
            '123abc'
        );
    }

    public function test_that_InvalidArgument_exception_is_thrown_on_invalid_rules(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new User(
            'test@test.de',
            'abcdef1234567890',
            ['unknown_rule'],
            '123abc'
        );
    }

    public function test_that_updateEmail_resets_verified_flag(): void
    {
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );
        $user->setEmailAddressVerified();
        $this->assertTrue($user->isEmailAddressVerified());

        $user->updateEmail('test@updated.de', 'abcd123');

        $this->assertSame($user->getEmail(), 'test@updated.de');
        $this->assertSame($user->getVerificationCode(), 'abcd123');
        $this->assertGreaterThan((new \DateTimeImmutable())->modify('-1 second'), $user->getVerificationCodeCreatedAt());
        $this->assertLessThan((new \DateTimeImmutable())->modify('+1 second'), $user->getVerificationCodeCreatedAt());
        $this->assertFalse($user->isEmailAddressVerified());
    }

    public function test_that_updatePassword_resets_passwordResetRequested_flag(): void
    {
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );
        $user->requestPasswordReset();
        $this->assertTrue($user->isPasswordResetRequested());

        $user->updatePassword('newHashedPw');
        $this->assertSame('newHashedPw', $user->getPassword());
        $this->assertFalse($user->isPasswordResetRequested());
    }

    public function test_that_adding_unknown_role_throws_InvalidArgumentException(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $user->addRole('invalid_role');
    }

    public function test_that_readding_an_existing_role_does_not_change_assigned_roles(): void
    {
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $user->addRole(User::ROLE_LOCATION_ADMIN);
        $this->assertEquals([User::ROLE_USER, User::ROLE_LOCATION_ADMIN], $user->getRoles());

        $user->addRole(User::ROLE_LOCATION_ADMIN);
        $this->assertEquals([User::ROLE_USER, User::ROLE_LOCATION_ADMIN], $user->getRoles());
    }

    public function test_that_removing_not_present_role_does_not_change_assigned_roles(): void
    {
        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $user->removeRole('unassigned_role');
        $this->assertEquals([User::ROLE_USER], $user->getRoles());
    }

    public function test_that_removing_the_only_assigned_role_throws_DomainException(): void
    {
        $this->expectException(\DomainException::class);

        $user = new User(
            'test@test.de',
            'abcdef1234567890',
            [User::ROLE_USER],
            '123abc'
        );

        $user->removeRole(User::ROLE_USER);
    }
}
