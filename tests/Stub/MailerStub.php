<?php

declare(strict_types=1);

namespace App\Tests\Stub;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\RawMessage;

class MailerStub implements MailerInterface
{
    private ?TemplatedEmail $sentMessage;

    public function send(RawMessage $message, Envelope $envelope = null): void
    {
        $this->sentMessage = $message;
    }

    public function getSentMessage(): ?TemplatedEmail
    {
        return $this->sentMessage;
    }
}
