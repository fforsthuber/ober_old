<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class ForgotPasswordTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_follow_forgot_password_link(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $crawler = $this->symfonyClient->clickLink('Passwort vergessen');

        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());
        $this->assertStringEndsWith('/de/forgotPassword/', $crawler->getUri());
    }

    public function test_forgot_password_fail_with_empty_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => '',
            ]
        );

        $this->assertStringContainsString(
            'Bitte eine Emailadresse angeben',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_forgot_password_fail_with_invalid_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'invalid@email',
            ]
        );

        $this->assertStringContainsString(
            'Wert ist keine gültige E-Mail-Adresse',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_forgot_password_fail_with_unknown_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'unknown@test.de',
            ]
        );

        $this->assertStringContainsString(
            'Diese Emailadresse ist nicht registriert',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_navigate_back_to_login(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $crawler = $this->symfonyClient->clickLink('Zurück zur Anmeldung');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
    }

    public function test_redirect_to_info_page_if_logged_in(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );

        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->assertSame(Response::HTTP_FOUND, $this->symfonyClient->getResponse()->getStatusCode());
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_forgot_password_success(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = $mailerStub->getSentMessage()->getContext()['resetPasswordLink'];

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertSame(
            'Wir haben dir einen Link geschickt um dein Passwort zurückzusetzen',
            $crawler->filter('.success_message .message-body')->text()
        );

        $crawler = $this->symfonyClient->request('GET', $resetPasswordLink);

        $this->assertStringContainsString('/de/resetPassword/', $crawler->getUri());
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());
    }
}
