<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class RegisterTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_register_fail_empty_form():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(200, $this->symfonyClient->getResponse()->getStatusCode());

        $crawler = $this->symfonyClient->submitForm('register-btn');
        $this->assertSame(
            'Dieser Wert sollte nicht leer sein.',
            $crawler->filter('#register_email')->parents()->parents()->children('p')->text()
        );
        $this->assertSame(
            'Dieser Wert sollte nicht leer sein.',
            $crawler->filter('#register_password_first')->parents()->parents()->children('p')->text()
        );
    }

    public function test_register_fail_invalid_email_address():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(200, $this->symfonyClient->getResponse()->getStatusCode());

        $crawler = $this->symfonyClient->submitForm('register-btn', ['register[email]' => 'test@test']);
        $this->assertSame(
            'Dieser Wert ist keine gültige E-Mail-Adresse.',
            $crawler->filter('#register_email')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('register-btn', ['register[email]' => str_repeat('a', 255) . '@test.de']);
        $this->assertSame(
            'Die Emailadresse darf nicht länger als 180 Zeichen sein',
            $crawler->filter('#register_email')->parents()->parents()->children('p')->text()
        );
    }

    public function test_register_fail_invalid_password():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(200, $this->symfonyClient->getResponse()->getStatusCode());

        $crawler = $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'test@test.de',
                'register[password][first]' => '',
                'register[password][second]' => ''
            ]
        );
        $this->assertSame(
            'Dieser Wert sollte nicht leer sein.',
            $crawler->filter('#register_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'test@test.de',
                'register[password][first]' => 'a',
                'register[password][second]' => 'a'
            ]
        );
        $this->assertSame(
            'Das Passwort muss mindestens 8 Zeichen haben',
            $crawler->filter('#register_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'test@test.de',
                'register[password][first]' => str_repeat('a', 256),
                'register[password][second]' => str_repeat('a', 256)
            ]
        );
        $this->assertSame(
            'Das Passwort darf nicht länger als 255 Zeichen sein',
            $crawler->filter('#register_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'test@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'bbbbbbbb'
            ]
        );
        $this->assertSame(
            'Die Passwörter stimmen nicht überein',
            $crawler->filter('#register_password_first')->parents()->parents()->children('p')->text()
        );
    }

    public function test_register_fail_duplicate_email_address():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(200, $this->symfonyClient->getResponse()->getStatusCode());

        $crawler = $this->symfonyClient->submitForm('register-btn', ['register[email]' => 'verified@test.de']);
        $this->assertSame(
            'Ein Nutzer mit dieser Emailadresse existiert bereits. Nutze die \'Passwort vergessen\' Funktion, falls du dich schon einmal hier regsistriert hast',
            $crawler->filter('#register_email')->parents()->parents()->children('p')->text()
        );
    }

    public function test_register_success():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(200, $this->symfonyClient->getResponse()->getStatusCode());

        $crawler = $this->symfonyClient->submitForm('register-btn',
            ['register[email]' => 'test@test.de', 'register[password][first]' => 'aaaaaaaa', 'register[password][second]' => 'aaaaaaaa']
        );
        $this->assertSame(
            'Danke, dass du dich registriert hast. Zur Verifizierung deiner Emailadresse haben wir dir einen Link geschickt',
            $crawler->filter('.success_message')->text()
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);

        $this->assertArrayHasKey('verificationLink', $mailerStub->getSentMessage()->getContext());
        $this->assertStringStartsWith(
            'http://localhost/de/verifyEmailAddress/',
            $mailerStub->getSentMessage()->getContext()['verificationLink']
        );
    }
}
