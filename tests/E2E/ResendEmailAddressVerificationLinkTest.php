<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class ResendEmailAddressVerificationLinkTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_resend_fail_with_empty_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'resend_verify_email_address_link[email]' => '',
            ]
        );

        $this->assertStringContainsString(
            'Bitte eine Emailadresse angeben',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_resend_fail_with_invalid_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'resend_verify_email_address_link[email]' => 'invalid@email',
            ]
        );

        $this->assertStringContainsString(
            'Wert ist keine gültige E-Mail-Adresse',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_resend_fail_with_unknown_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'resend_verify_email_address_link[email]' => 'unknown@test.de',
            ]
        );

        $this->assertStringContainsString(
            'Diese Emailadresse ist nicht registriert',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_resend_fail_with_already_verified_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $crawler = $this->symfonyClient->submitForm('request-link-btn',
            [
                'resend_verify_email_address_link[email]' => 'verified@test.de',
            ]
        );

        $this->assertStringContainsString(
            'Diese Emailadresse ist schon verifiziert',
            $crawler->filter('p.is-danger')->text()
        );
    }

    public function test_navigate_back_to_login(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $crawler = $this->symfonyClient->clickLink('Zurück zur Anmeldung');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
    }

    public function test_redirect_to_info_page_if_logged_in(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );

        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $this->assertSame(Response::HTTP_FOUND, $this->symfonyClient->getResponse()->getStatusCode());
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_resend_success(): void
    {
        $this->symfonyClient->request('GET', '/de/resendVerifyEmailAddressLink/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'resend_verify_email_address_link[email]' => 'unverified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertSame(
            'Es wurde dir ein neuer Verifizierungslink zugeschickt',
            $crawler->filter('.success_message .message-body')->text()
        );

        $this->symfonyClient->request('GET', $verificationLink);
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }
}
