<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Manipulator\UserManipulator;
use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class VerifyEmailAddressTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_verification_success(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'verification_success@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'aaaaaaaa'
            ]
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];

        $this->symfonyClient->request('GET', $verificationLink);
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_reenter_verification_link_has_no_effect_when_logged_in(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'reenter_verification_link@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'aaaaaaaa'
            ]
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];

        $this->symfonyClient->request('GET', $verificationLink);
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->request('GET', $verificationLink);
        $this->symfonyClient->followRedirect();
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_reenter_verification_link_warning_when_not_logged_in(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'reenter_verification_link2@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'aaaaaaaa'
            ]
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];

        $this->symfonyClient->request('GET', $verificationLink);
        $this->symfonyClient->request('GET', '/logout/');


        $this->symfonyClient->request('GET', $verificationLink);
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
        $this->assertSame(
            'Deine Email Adresse wurde bereits verifiziert',
            $crawler->filter('.message-body')->text()
        );

        $this->assertCount(0, $crawler->filter('.message-body a'), 'resendVerifyEmailAddressLink is not shown');
    }

    public function test_verification_fail_with_invalid_verification_code(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'invalid_verification_code@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'aaaaaaaa'
            ]
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $invalidVerificationLink =
            str_replace(
                'verifyEmailAddress/',
                'verifyEmailAddress/invalid',
                $mailerStub->getSentMessage()->getContext()['verificationLink']
            );

        $this->symfonyClient->request('GET', $invalidVerificationLink);
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/login/?showResendEmailAddressVerificationLink=1', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
        $this->assertSame(
            'Dein Email Verifizierungscode ist ungültig',
            $crawler->filter('.error_message .message-body')->text()
        );
        $this->assertSame('/de/resendVerifyEmailAddressLink/', $crawler->filter('.message-body a')->attr('href'));
    }

    public function test_verification_fail_with_outdated_verification_code(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm('register-btn',
            [
                'register[email]' => 'outdated_verification_code@test.de',
                'register[password][first]' => 'aaaaaaaa',
                'register[password][second]' => 'aaaaaaaa'
            ]
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];
        $this->symfonyClient
            ->getContainer()
            ->get(UserManipulator::class)
            ->modifyVerficationCodeCreatedAt('outdated_verification_code@test.de', '-1 year')
        ;
        $this->symfonyClient->request('GET', $verificationLink);
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/login/?showResendEmailAddressVerificationLink=1', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
        $this->assertSame(
            'Dein Email Verifizierungscode ist nicht mehr gültig',
            $crawler->filter('.error_message .message-body')->text()
        );
        $this->assertSame('/de/resendVerifyEmailAddressLink/', $crawler->filter('.message-body a')->attr('href'));
    }
}
