<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Panther\PantherTestCase;

class LoginTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_login_fail_empty_form(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());

        $this->symfonyClient->submitForm('login-btn');
        $crawler = $this->symfonyClient->followRedirect();
        $errorMessage = $crawler->filter('#login-error-message')->text('');
        $this->assertSame('Emailadresse oder Passwort ist falsch', $errorMessage);

        $this->symfonyClient->request('GET', '/de/info/');

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }

    public function test_login_fail_wrong_credentials(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'unknown@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $errorMessage = $crawler->filter('#login-error-message')->text('');
        $this->assertSame('Emailadresse oder Passwort ist falsch', $errorMessage);

        $this->symfonyClient->request('GET', '/de/info/');

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }

    public function test_login_fail_inactive_user(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'deactivated@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $errorMessage = $crawler->filter('#login-error-message')->text('');
        $this->assertSame('Ihr Account ist gesperrt. Bitte wenden sie sich an unseren Support', $errorMessage);

        $this->symfonyClient->request('GET', '/de/info/');

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }

    public function test_login_fail_unverified_email_address(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'unverified@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertSame('Bitte verifiziere deine Emailadresse.', $crawler->filter('#login-error-message')->text(''));

        $emailAddressResentHintText = $crawler->filter('#email-address-resent-hint')->text('');
        $this->assertSame('Verifizierungslink nochmal versenden ?', $emailAddressResentHintText);
        $this->assertSame('a', $crawler->filter('#email-address-resent-hint')->children()->getNode(0)->tagName);

        $this->symfonyClient->request('GET', '/de/info/');

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }

    public function test_login_success(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_logout_success(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Abmelden');
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }
}
