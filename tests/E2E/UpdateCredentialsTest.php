<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class UpdateCredentialsTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_redirect_to_login_if_no_session(): void
    {
        $this->symfonyClient->request('GET', '/de/updateCredentials/');
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
    }

    public function test_back_to_info_link(): void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');
        $crawler = $this->symfonyClient->clickLink('Zurück');
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
    }

    public function test_sending_empty_form_does_not_change_credentials():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');

        $this->symfonyClient->submitForm('update-btn');
        $this->symfonyClient->clickLink('Abmelden');
        $this->symfonyClient->followRedirect();

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_update_fail_invalid_email_address():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');

        $crawler = $this->symfonyClient->submitForm(
            'update-btn',
            ['update_credentials[email]' => 'test@test']
        );
        $this->assertSame(
            'Dieser Wert ist keine gültige E-Mail-Adresse.',
            $crawler->filter('#update_credentials_email')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm(
            'update-btn',
            ['update_credentials[email]' => str_repeat('a', 255) . '@test.de']
        );
        $this->assertSame(
            'Die Emailadresse darf nicht länger als 180 Zeichen sein',
            $crawler->filter('#update_credentials_email')->parents()->parents()->children('p')->text()
        );
    }

    public function test_update_fail_invalid_password():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');

        $crawler = $this->symfonyClient->submitForm('update-btn',
            [
                'update_credentials[password][first]' => 'a',
                'update_credentials[password][second]' => 'a'
            ]
        );
        $this->assertSame(
            'Das Passwort muss mindestens 8 Zeichen haben',
            $crawler->filter('#update_credentials_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('update-btn',
            [
                'update_credentials[password][first]' => str_repeat('a', 256),
                'update_credentials[password][second]' => str_repeat('a', 256)
            ]
        );
        $this->assertSame(
            'Das Passwort darf nicht länger als 255 Zeichen sein',
            $crawler->filter('#update_credentials_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('update-btn',
            [
                'update_credentials[password][first]' => 'aaaaaaaa',
                'update_credentials[password][second]' => 'bbbbbbbb'
            ]
        );
        $this->assertSame(
            'Die Passwörter stimmen nicht überein',
            $crawler->filter('#update_credentials_password_first')->parents()->parents()->children('p')->text()
        );
    }

    public function test_update_email_success():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');

        $this->symfonyClient->submitForm(
            'update-btn',
            ['update_credentials[email]' => 'updated@test.de']
        );

        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $verificationLink = $mailerStub->getSentMessage()->getContext()['verificationLink'];

        $this->symfonyClient->followRedirect();
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/login/?emailAddressChanged=1', $crawler->getUri());
        $this->assertCount(0, $crawler->filter('#logout-btn'));
        $this->assertSame(
            'Du hast deine Emailadresse geändert. Wir haben dir einen neuen Verifizierungslink zugeschickt',
            $crawler->filter('.success_message')->text()
        );

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'updated@test.de', 'password' => 'test1234']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertSame('Bitte verifiziere deine Emailadresse.', $crawler->filter('#login-error-message')->text(''));

        $this->symfonyClient->request('GET', $verificationLink);
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }

    public function test_update_password_success():void
    {
        $this->symfonyClient->request('GET', '/de/login/');
        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'test1234']
        );
        $this->symfonyClient->followRedirect();
        $this->symfonyClient->clickLink('Zugangsdaten');

        $crawler = $this->symfonyClient->submitForm(
            'update-btn',
            [
                'update_credentials[password][first]' => 'bbbbbbbb',
                'update_credentials[password][second]' => 'bbbbbbbb'
            ]
        );

        $this->assertSame(
            'Deine Zugangsdaten wurden aktualisiert',
            $crawler->filter('.message-body')->text()
        );

        $this->symfonyClient->clickLink('Abmelden');
        $this->symfonyClient->followRedirect();

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'bbbbbbbb']
        );

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }
}
