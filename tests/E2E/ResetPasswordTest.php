<?php

declare(strict_types=1);

namespace App\Tests\E2E;

use App\Tests\Manipulator\UserManipulator;
use App\Tests\Stub\MailerStub;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Panther\PantherTestCase;

class ResetPasswordTest extends PantherTestCase
{
    use FixturesTrait;

    /** @var KernelBrowser  */
    private $symfonyClient;

    protected function setUp(): void
    {
        $this->symfonyClient = static::createClient();
        $this->loadFixtures([
            'App\Tests\Fixture\UsersFixture'
        ]);
    }

    public function test_reset_fail_with_outdated_verification_code(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = $mailerStub->getSentMessage()->getContext()['resetPasswordLink'];

        $this->symfonyClient
            ->getContainer()
            ->get(UserManipulator::class)
            ->modifyVerficationCodeCreatedAt('verified@test.de', '-1 year')
        ;

        $this->symfonyClient->request('GET', $resetPasswordLink);
        $crawler = $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => 'aaaaaaaa',
                'reset_password[password][second]' => 'aaaaaaaa'
            ]
        );

        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertStringContainsString(
            'Der Code zum zurücksetzen deines Passwortes ist abgelaufen',
            $crawler->filter('.error_message .message-body')->text()
        );
    }

    public function test_reset_fail_with_invalid_verification_code(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = str_replace(
            'resetPassword/',
            'resetPassword/INVALID',
            $mailerStub->getSentMessage()->getContext()['resetPasswordLink']
        );

        $this->symfonyClient->request('GET', $resetPasswordLink);
        $crawler = $this->symfonyClient->followRedirect();

        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
        $this->assertStringContainsString(
            'Der Code zum zurücksetzen deines Passwortes ist ungültig',
            $crawler->filter('.error_message .message-body')->text()
        );
    }

    public function test_reset_fail_invalid_password():void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = $mailerStub->getSentMessage()->getContext()['resetPasswordLink'];

        $this->symfonyClient->request('GET', $resetPasswordLink);

        $crawler = $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => '',
                'reset_password[password][second]' => ''
            ]
        );
        $this->assertSame(
            'Dieser Wert sollte nicht leer sein.',
            $crawler->filter('#reset_password_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => 'a',
                'reset_password[password][second]' => 'a'
            ]
        );
        $this->assertSame(
            'Das Passwort muss mindestens 8 Zeichen haben',
            $crawler->filter('#reset_password_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => str_repeat('a', 256),
                'reset_password[password][second]' => str_repeat('a', 256),
            ]
        );
        $this->assertSame(
            'Das Passwort darf nicht länger als 255 Zeichen sein',
            $crawler->filter('#reset_password_password_first')->parents()->parents()->children('p')->text()
        );

        $crawler = $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => 'aaaaaaaa',
                'reset_password[password][second]' => 'bbbbbbbb',
            ]
        );
        $this->assertSame(
            'Die Passwörter stimmen nicht überein',
            $crawler->filter('#reset_password_password_first')->parents()->parents()->children('p')->text()
        );
    }

    public function test_navigate_back_to_login():void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = $mailerStub->getSentMessage()->getContext()['resetPasswordLink'];

        $this->symfonyClient->request('GET', $resetPasswordLink);
        $crawler = $this->symfonyClient->clickLink('Zurück zur Anmeldung');

        $this->assertSame(Response::HTTP_OK, $this->symfonyClient->getResponse()->getStatusCode());
        $this->assertStringEndsWith('/de/login/', $crawler->getUri());
    }

    public function test_reset_success(): void
    {
        $this->symfonyClient->request('GET', '/de/forgotPassword/');
        $this->symfonyClient->submitForm('request-link-btn',
            [
                'forgot_password[email]' => 'verified@test.de',
            ]
        );
        /** @var MailerStub $mailerStub */
        $mailerStub = $this->symfonyClient->getContainer()->get(MailerInterface::class);
        $resetPasswordLink = $mailerStub->getSentMessage()->getContext()['resetPasswordLink'];

        $this->symfonyClient->request('GET', $resetPasswordLink);

        $this->symfonyClient->submitForm('reset-btn',
            [
                'reset_password[password][first]' => 'aaaaaaaa',
                'reset_password[password][second]' => 'aaaaaaaa'
            ]
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));

        //try to login with new password
        $this->symfonyClient->clickLink('Abmelden');
        $this->symfonyClient->followRedirect();

        $this->symfonyClient->submitForm(
            'login-btn',
            ['email' => 'verified@test.de', 'password' => 'aaaaaaaa']
        );
        $crawler = $this->symfonyClient->followRedirect();
        $this->assertStringEndsWith('/de/info/', $crawler->getUri());
        $this->assertCount(1, $crawler->filter('#logout-btn'));
    }
}
